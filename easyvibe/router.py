from rest_framework.routers import Route,SimpleRouter

class EasyvibeRouter(SimpleRouter):
    """
    A router for easyvibe API
    """

    routes = [
        Route(url=r'^{prefix}$',
              mapping={'get':'fetch','post':'create','put':'update'},
              name='{basename}-get'),
        Route(url=r'^{prefix}/{lookup}$',
              mapping={'get':'retrieve','put':'update_exact'},
              name='{basename}-detailed')
    ]
