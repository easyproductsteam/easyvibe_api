"""
Django settings for easyvibe project.

For more information on this file, see
https://docs.djangoproject.com/en/1.6/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.6/ref/settings/
"""

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
import os
import djcelery

BASE_DIR = os.path.dirname(os.path.dirname(__file__))


# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.6/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = '*mnrl6_z=qyf-7woi0d*lgl#w3qx(+f%iqk+v%n6__#ynq&(8s'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

TEMPLATE_DEBUG = True

ALLOWED_HOSTS = ['api.easyvibe.ru','0.0.0.0']

# Application definition

INSTALLED_APPS = (
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'gunicorn',
    'social_auth',
    'rest_framework',
    'evb_rest',
    'djcelery',
    'kombu.transport.django',
    #'south',
)

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
)

REST_FRAMEWORK = {
    'DEFAULT_MODEL_SERIALIZER_CLASS':'rest_framework.serializers.HyperlinkedModelSerializer'
}

ROOT_URLCONF = 'easyvibe.urls'

WSGI_APPLICATION = 'easyvibe.wsgi.application'
#
# LOGGING = {
#     'version': 1,
#     'disable_existing_loggers': True
# }

LOGGING = {
    'version': 1,
    'disable_existing_loggers': True,
    'formatters': {
        'standard': {
            'format' : "[%(asctime)s] %(levelname)s [%(filename)s:%(lineno)s] %(message)s",
            'datefmt' : "%d/%b/%Y %H:%M:%S"
        },
    },
    'handlers': {
        'null': {
            'level':'DEBUG',
            'class':'django.utils.log.NullHandler',
        },
        'logfile': {
            'level':'DEBUG',
            'class':'logging.handlers.RotatingFileHandler',
            'filename': BASE_DIR + "/log/logfile.log",
            'maxBytes': 50000,
            'backupCount': 15,
            'formatter': 'standard',
        },
        'console':{
            'level':'INFO',
            'class':'logging.StreamHandler',
            'formatter': 'standard'
        },
    },
    'loggers': {
        'django': {
            'handlers':['console'],
            'propagate': True,
            'level':'WARN',
        },
        'django.db.backends': {
            'handlers': ['console'],
            'level': 'DEBUG',
            'propagate': False,
        },
        'web_logger': {
            'handlers': ['logfile'],
            'level': 'DEBUG',
        },
    }
}
# Database
# https://docs.djangoproject.com/en/1.6/ref/settings/#databases
# 'PASSWORD':'mysQlpass7',
# yJRBL4MT4zX5NEuc
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'easyvibe_db',
        'USER':'evb',
        'PASSWORD':'yJRBL4MT4zX5NEuc',
        'HOST':'localhost',
        'PORT':'3306',
    }
}

CELERYBEAT_SCHEDULER = "djcelery.schedulers.DatabaseScheduler"
djcelery.setup_loader()


BROKER_BACKEND = "djkombu.transport.DatabaseTransport"

#BROKER_URL = 'django://'

# Internationalization
# https://docs.djangoproject.com/en/1.6/topics/i18n/


LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'Europe/Moscow'

USE_I18N = True

USE_L10N = True

USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.6/howto/static-files/
# if DEBUG:
#     DOMAIN_HOME = 'http://freeform-family.ru:8000'
# else:
#     DOMAIN_HOME = 'http://api.easyvibe.ru'

#time to refresh vibe counter for one step, in seconds
ZERO_INTERVAL = '00:01'
STEP1_INTERVAL = '00:20'
STEP2_INTERVAL = '00:20'
STEP3_INTERVAL = '00:20'
STEP4_INTERVAL = '00:20'

VIBE_REFRESH_INTERVAL = 3600
VIBES_MAX_QTY = 50
MULTIVIBE_USERS = 25
INVITE_SHOW_BONUS = 25
LATEST_VERSION = '2.0.3'

SOUNDCLOUD_CLIENT_ID = 'ea1a30fa9a10f65cc4761015bfe32736'
DOMAIN_HOME='http://api.easyvibe.ru'

APP_STORE_URL = 'https://itunes.apple.com/ru/app/easyvibe-meet-people-who-share/id787681885'
STATIC_ROOT = '/var/easyvibe/data/static/'
SONG_ROOT = STATIC_ROOT + "songs/"
STATIC_URL = '/static/'
APS_CERT_PATH_PRODUCTION = os.path.join(STATIC_ROOT, 'certs', 'iphone_prod.pem')
APS_CERT_APPSTORE = os.path.join(STATIC_ROOT, 'certs', 'app_store_prod.pem')
APS_CERT_PATH_DEVELOPMENT = os.path.join(STATIC_ROOT, 'certs', 'iphone_devel.pem')
APS_CERT_ADHOOK_DEVELOPMENT = os.path.join(STATIC_ROOT, 'certs', 'adhook-test.pem')
APS_CERT_ADHOOK_PRODUCTION = os.path.join(STATIC_ROOT, 'certs', 'adhook-prod.pem')