from django.conf import settings
from django.conf.urls.static import static
from rest_framework import routers
from django.contrib import admin
from evb_rest.logic.invite import InviteViewSet
from evb_rest.logic.login import LoginViewSet
from evb_rest.logic.user import UserViewSet
from evb_rest.logic.dialog import DialogViewSet
from evb_rest.logic.picture import PictureViewSet
from evb_rest.logic.song import SongViewSet
from evb_rest.logic.songmeta import SongMetaViewSet
from evb_rest.logic.recommendation import RecommendationViewSet
from evb_rest.logic.device import DeviceViewSet
from evb_rest.logic.v2.device import DeviceViewSetV2
from evb_rest.logic.v2.dialog import DialogViewSetV2
from evb_rest.logic.v2.invite import InviteViewSetV2
from evb_rest.logic.v2.login import LoginViewSetV2
from evb_rest.logic.v2.picture import PictureViewSetV2
from evb_rest.logic.v2.recommendation import RecommendationViewSetV2
from evb_rest.logic.v2.song import SongViewSetV2
from evb_rest.logic.v2.songmeta import SongMetaViewSetV2
from evb_rest.logic.v2.user import UserViewSetV2
from evb_rest.logic.v2.vibe import VibeViewSetV2, MultivibeViewSetV2
from evb_rest.logic.vibe import VibeViewSet, MultivibeViewSet
from evb_rest.viewsets import *


admin.autodiscover()

router = routers.SimpleRouter()
router.register(r'v1/user/auth', LoginViewSet)
router.register(r'v1/user', UserViewSet)
router.register(r'v1/dialog', DialogViewSet)
router.register(r'v1/picture', PictureViewSet)
router.register(r'v1/song', SongViewSet)
router.register(r'v1/songmeta', SongMetaViewSet)
router.register(r'v1/recommendation', RecommendationViewSet)
router.register(r'v1/device', DeviceViewSet)
router.register(r'v1/itunes', ItunesViewSet)
router.register(r'v1/vibes', VibeViewSet)
router.register(r'v1/multivibe', MultivibeViewSet)
router.register(r'v1/invite', InviteViewSet)

#V2
router.register(r'v2/user/auth', LoginViewSetV2)
router.register(r'v2/user', UserViewSetV2)
router.register(r'v2/dialog', DialogViewSetV2)
router.register(r'v2/picture', PictureViewSetV2)
router.register(r'v2/song', SongViewSetV2)
router.register(r'v2/songmeta', SongMetaViewSetV2)
router.register(r'v2/recommendation', RecommendationViewSetV2)
router.register(r'v2/device', DeviceViewSetV2)
router.register(r'v2/vibes', VibeViewSetV2)
router.register(r'v2/multivibe', MultivibeViewSetV2)
router.register(r'v2/invite', InviteViewSetV2)

urlpatterns = router.urls + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)

