__author__ = 'Usachev'

from apns import *

from evb_rest.utils import log2console
from evb_rest.models import Notification, User


def schedule_profile_notification_v1(user_id, device_id):
    log2console('Running Profile Notification task')
    # creating a PUSH message
    push_args = {}
    push_args['loc_key'] = 'NEW_PACKET_KEY'
    n = Notification(device_id=device_id, push_args=json.dumps(push_args))
    n.save()
    try:
        u = User.objects.get(id=int(user_id))
        u.is_notified = True
        u.save(force_update=True)
    except User.DoesNotExist:
        log2console('DB violation: user %d does not exist!!!' % user_id)
