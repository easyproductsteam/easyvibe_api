from django.shortcuts import render

# Create your views here.
from rest_framework import generics
from rest_framework.decorators import api_view
from rest_framework.reverse import reverse
from rest_framework.response import Response
from serializers import UserSerializer
from models import User

@api_view(['GET,POST'])
def user_list(request):

    if request.method == 'GET':
        users = User.select(lambda u:u.id>0)
        serializer = UserSerializer(users,many=False)
        return Response(serializer.data)

# @api_view(['POST'])
# def api_auth(request,format=None):
#     '''
#     Entry point of API Service
#     '''
#     print 'User comes in!'
#     u = User()
#     u.id=1
#     u.name='Usachev'
#     u.age=26
#     return Response({'user':u})