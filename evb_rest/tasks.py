from celery import Celery
from celery.task import periodic_task
from celery.schedules import crontab
from dateutil.relativedelta import relativedelta
from django.conf import settings
from apns import *
import pymssql
from urllib2 import urlopen
from django.db import connection
from random import randint, seed, shuffle
from distutils.version import StrictVersion as V

from evb_rest.legacy.utils import schedule_profile_notification_v1
from evb_rest.utils import log2console, save_avatar, analyze_playlist, get_song, create_sql_clause_list
from models import create_song_hash, SONG_GENRES, Song, Notification, Device, User, OfferSet, Offer, CalcDistance
from easyvibe.settings import APS_CERT_PATH_PRODUCTION, INVITE_SHOW_BONUS, APS_CERT_ADHOOK_PRODUCTION








# os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'easyvibe.settings')
app = Celery('easyvibe')
app.config_from_object('django.conf:settings')
app.autodiscover_tasks(lambda: settings.INSTALLED_APPS)


@app.task(ignore_result=True)
def upload_songs_async(user, playlist):
    if playlist is not None:
        for track in playlist:
            author = track['artist'].encode('utf-8')
            title = track['title'].encode('utf-8')
            song_hash = create_song_hash(author, title)
            if 'genre' in list(track.keys()):
                song_genre = SONG_GENRES[track['genre']]
            else:
                song_genre = SONG_GENRES[18]
            s = Song(author=author,
                     genre=song_genre,
                     title=title,
                     url=track['url'],
                     hash=song_hash,
                     vk_uid=track['aid']
            )
            old_song = Song.objects.filter(hash=song_hash).first()
            if old_song is None:
                s.save()
                user.songs.add(s)
            else:
                user.songs.add(old_song)


@periodic_task(run_every=crontab(minute='*/1'))
def schedule_profile_notification():
    log2console('Running Profile Notification task')
    dbcur = connection.cursor()
    #todo check and change query to create packages for all users
    dbcur.execute('''
        SELECT ed.id, eu.id
        FROM evb_devices AS ed
        JOIN evb_users AS eu ON eu.id = ed.user_id
        WHERE token IS NOT NULL AND eu.is_notified = 0  and eu.do_multivibe = 0
        AND time(from_unixtime(unix_timestamp(now()) - unix_timestamp(eu.vibe_used_at))) > recharge_timeout
        ''')
    #AND time(from_unixtime(unix_timestamp(now()) - unix_timestamp(eu.vibe_used_at))) > recharge_timeout
    # for all devices that's need to be notified...
    for row in dbcur.fetchall():
        device_id = row[0]
        user_id = int(row[1])

        #detecting operating client version
        dev = Device.objects.get(id=device_id)
        if (dev.client_version is not None) and (V(dev.client_version) <= V('2.0.2')):
            log2console('Client version: %s' % dev.client_version)
            #if V(dev.client_version) <= V('2.0.2'):
            log2console('Building package for legacy version')
            schedule_profile_notification_v1(user_id,device_id)
        else:
            # request an offer for user
            packages = OfferSet.objects.filter(user_id=user_id, used_at__isnull=True).count()
            if packages>0:
                #log2console('User has unused package, skipping user...')
                continue
            request_offer(user_id)
            # creating a PUSH message
            push_args = {}
            push_args['loc_key'] = 'NEW_PACKET_KEY'
            n = Notification(device_id=device_id, push_args=json.dumps(push_args))
            n.save()
            try:
                u = User.objects.get(id=int(user_id))
                u.is_notified = True
                u.save(force_update=True)
            except User.DoesNotExist:
                log2console('DB violation: user %d does not exist!!!' % user_id)
    dbcur.close()


def request_award_offers(user_pk, lim=25):
    log2console('Requesting an award offer for: %d' % user_pk)
    log2console('Check available offer set...')
    packages_qty = OfferSet.objects.filter(user_id=user_pk, type=2, used_at__isnull=True).order_by('id').count()
    user = User.objects.get(id=user_pk)
    if packages_qty > 0:
        log2console('Special offer already exist')
    else:
        if lim is None:
            total_users = INVITE_SHOW_BONUS
        else:
            total_users = lim
        div_left = total_users % 4
        part_size = total_users / 4
        result_list = []

        #build old unshown users
        query = '''
        SELECT id
FROM evb_users
where gender != %d
and language='%s'
and (facebook_id is not null or vk_id is not null)
AND created_at > DATE_ADD(NOW(),INTERVAL -2 DAY)
AND id >= FLOOR(RAND()* (select max(id) from evb_users))
LIMIT 0,%d''' % (user.gender,user.language,part_size)

# AND id NOT
#  IN (
#  SELECT user_id
#  FROM evb_user_transations
#  WHERE owner_id =%d
#  )

        log2console('OLD UNSHOWN USERS SQL QUERY DEBUG: %s' % query)
        old_ids = [u.id for u in User.objects.raw(query)]
        result_list = old_ids

        #build old users, that was already shown
        where_clause = create_sql_clause_list(result_list)
        query = '''select id FROM evb_users
        where created_at > DATE_ADD(NOW(),INTERVAL -2 DAY)
        and id not in %s
        and language = '%s'
        and gender != '%s'
        and (facebook_id is not null or vk_id is not null)
        AND id >= FLOOR(RAND()* (select max(id) from evb_users))
        limit 0, %d''' % (where_clause, user.language,user.gender, part_size)

        log2console('OLD ALREADY SHOWN USERS SQL QUERY DEBUG: %s' % query)
        #old_shown_ids = [t.user_id for t in  Transaction.objects.raw(query)]
        old_shown_ids = [u.id for u in  User.objects.raw(query)]
        result_list = result_list+old_shown_ids


        #build new users,
        where_clause = create_sql_clause_list(result_list)
        query = '''select distinct id
        from evb_users
        where id not in
        (select user_id from evb_user_transations where owner_id=%d)
        and created_at < DATE_ADD(NOW(),INTERVAL -2 DAY)
        and gender!=%d
        and language='%s'
        and (facebook_id is not null or vk_id is not null)
        and  id not in %s
        AND id >= FLOOR(RAND()* (select max(id) from evb_users))
        limit 0, %d''' % (
        user_pk, user.gender, user.language, where_clause, part_size)

        log2console('NEW USERS SQL QUERY DEBUG: %s' % query)
        new_ids = [u.id for u in User.objects.raw(query)]
        result_list =  result_list+new_ids

        #build bots
        where_clause = create_sql_clause_list(result_list)
        query = '''
        select distinct id from evb_users where
        facebook_id is NULL
        AND vk_id is NULL
        and gender != %d
        and language='%s'
        and id not in %s
        AND id >= FLOOR(RAND()* (select max(id) from evb_users))
        limit 0, %d
        ''' % (user.gender, user.language, where_clause, part_size+div_left)

        log2console('BOT SQL QUERY DEBUG: %s' % query)
        bot_ids = [u.id for u in User.objects.raw(query)]
        result_list = result_list+bot_ids

        #shuffle and save the result...
        seed()
        st = OfferSet(user_id=user_pk, type=2, qty=len(result_list))
        st.save()
        shuffle(result_list)
        for o in result_list:
            offer = Offer(oset_id=st.id, person_id=o)
            offer.save()




def request_offer(user_pk, lim=None, in_test_case=False):
    log2console('Requesting package for: %d' % user_pk)
    dbcur = connection.cursor()

    if not in_test_case:
        log2console('Check available offer set...')
        packages_qty = OfferSet.objects.filter(user_id=user_pk, used_at__isnull=True).order_by('id').count()
        if packages_qty > 0:
            return
    else:
        log2console('Unit-test is in progress - existing package check ignored')

    current_user = User.objects.get(id=user_pk)
    dev_lat = 0
    dev_long = 0

    try:
        current_device = Device.objects.get(user_id = current_user.id)
        if (current_device.lat is not None) and (current_device.long is not None):
            dev_lat = current_device.lat
            dev_long = current_device.long
            geo_provided = True
        else:
            geo_provided = False
    except Device.DoesNotExist:
        geo_provided = False

    suggestions = []
    privileged_users = []

    # First of all,find Users who have used invitations
    log2console('Finding Users who have used invitations')
    ranks = [r.genre_id for r in current_user.current_ranks.order_by('-rank').all()]
    dbcur.execute('''SELECT min(evb_user_invitations.id ) , user_id, show_qty
        FROM easyvibe_db.evb_user_invitations
        INNER JOIN evb_users ON user_id = evb_users.id
        WHERE used_at IS NOT NULL AND evb_users.gender != %d and evb_users.language = '%s';''' % (current_user.gender,
                  current_user.language))
    for inv in dbcur.fetchall():
        if inv[0] is None:
            break
        row_id = int(inv[0])
        user_id = int(inv[1])
        show_qty = int(inv[2])
        try:
            user = User.objects.get(id=user_id)
            dist = CalcDistance.GetDistanceInKm(current_user, user)
            privileged_users.append({'user':user, 'distance':dist})
            # decrease a counter of User's privileged showing
            show_qty -= 1
            if show_qty < 1:
                dbcur.execute('''UPDATE evb_user_invitations SET show_qty = 0, used_at = NOW() WHERE id=%d''' % row_id)
            else:
                dbcur.execute('''UPDATE evb_user_invitations SET show_qty = %d WHERE id=%d''' % (show_qty, row_id))
        except User.DoesNotExist:
            pass
    log2console('Privileged users found: %d' % len(privileged_users))

    log2console('Generating package size, using random seed')

    if lim is None:
        seed()
        package_size = randint(5, 15)
    else:
        package_size = lim

    total_bots = package_size * 0.2
    total_users = package_size - total_bots - len(privileged_users)
    log2console("Finding %d people" % package_size)
    log2console("%d bot(s) will be shown" % total_bots)
    log2console("%d real user(s) will be shown" % total_users)

    if geo_provided:
        query = '''SELECT evb_users.id, round(CalculateDistanceKm(`lat`,`long`,%f,%f)) as D FROM evb_users
                        INNER JOIN evb_devices on evb_users.id = evb_devices.user_id
                        WHERE gender != %d and language='%s'
                        AND facebook_id IS NULL AND vk_id IS NULL
                        AND evb_users.id >= FLOOR(RAND()* (select max(id) from evb_users where facebook_id IS NULL AND vk_id IS NULL))
                        ORDER by D
                        LIMIT %d''' % (
           dev_lat,dev_long, current_user.gender,current_user.language,total_bots)
    else:
        query = '''SELECT evb_users.id FROM evb_users
                        WHERE gender != %d and language='%s'
                        AND facebook_id IS NULL AND vk_id IS NULL
                        AND id >= FLOOR(RAND()* (select max(id) from evb_users where facebook_id IS NULL AND vk_id IS NULL)) LIMIT %d''' % (
            current_user.gender,current_user.language,total_bots)
    dbcur.execute(query)
    results = dbcur.fetchall()
    for person in results:
        id = person[0]
        if geo_provided:
            dist = person[1]
        else:
            dist = 0
        try:
            u = User.objects.get(id=id)
            suggestions.append({'user':u, 'distance':dist})
        except User.DoesNotExist:
            pass
    log2console("%s bot(s) added" % len(suggestions))

    # selecting vibes using genre weight and already introduced interval
    for r in ranks:
        if geo_provided:
            query = '''
                            SELECT evb_users.id,round(CalculateDistanceKm(`lat`,`long`,%f,%f)) as D
                            FROM evb_users
                            INNER JOIN evb_devices on evb_users.id = evb_devices.user_id
                            INNER JOIN evb_genre_ranks ON evb_genre_ranks.user_id = evb_users.id
                            LEFT JOIN evb_dialogs ON evb_dialogs.user_id = evb_users.id
                            WHERE age >=%d
                            AND age <%d
                            AND gender != %d
                            AND genre_id =%d
                            AND `language` = '%s'
                            AND evb_dialogs.id IS NULL
                            AND evb_users.id NOT IN (
                            select user_id FROM evb_user_transations WHERE owner_id=%d AND created_at > DATE_ADD(NOW(),INTERVAL -2 DAY)
                            )
                            AND evb_users.id >= FLOOR(RAND()* (select max(id) from evb_users))
                            LIMIT 0,%d ''' % (
                dev_lat,
                dev_long,
                current_user.age - 4,
                current_user.age + 4,
                current_user.gender,
                r,
                current_user.language,
                current_user.id,
                total_users)
        else:
            query = '''
                            SELECT evb_users.id FROM evb_users
                            INNER JOIN evb_genre_ranks ON evb_genre_ranks.user_id = evb_users.id
                            LEFT JOIN evb_dialogs ON evb_dialogs.user_id = evb_users.id
                            WHERE age >=%d
                            AND age <%d
                            AND gender != %d
                            AND genre_id =%d
                            AND `language` = '%s'
                            AND evb_dialogs.id IS NULL
                            AND evb_users.id NOT IN (
                            select user_id FROM evb_user_transations WHERE owner_id=%d AND created_at > DATE_ADD(NOW(),INTERVAL -2 DAY)
                            )
                            AND evb_users.id >= FLOOR(RAND()* (select max(id) from evb_users))
                            LIMIT 0,%d ''' % (
                current_user.age - 4,
                current_user.age + 4,
                current_user.gender,
                r,
                current_user.language,
                current_user.id,
                total_users)
        log2console('Executing query: %s' % query)
        dbcur.execute(query)
        for person in dbcur.fetchall():
            id = person[0]
            if geo_provided:
                dist = person[1]
            else:
                dist = 0
            try:
                u = User.objects.get(id=id)
                suggestions.append({'user':u, 'distance':dist})
            except User.DoesNotExist:
                pass
        log2console("%s user(s) found" % len(suggestions))
        if len(suggestions) >= total_users:
            break

    if len(suggestions) < total_users:
        log2console('Not enough users was found, using filterless search...')
        if geo_provided:
            query = '''
                            SELECT evb_users.id, round(CalculateDistanceKm(`lat`,`long`,%f,%f)) as D
                            FROM evb_users
                            INNER JOIN evb_devices on evb_users.id = evb_devices.user_id
                            INNER JOIN evb_genre_ranks ON evb_genre_ranks.user_id = evb_users.id
                            LEFT JOIN evb_dialogs ON evb_dialogs.user_id = evb_users.id
                            WHERE age >=%d
                            AND age <%d
                            AND gender != %d
                            AND `language` = '%s'
                            AND (facebook_id IS NOT NULL OR vk_id IS NOT NULL)
                            AND evb_dialogs.id IS NULL
                            AND evb_users.id >= FLOOR(RAND()* (select max(id) from evb_users))
                            LIMIT 0,%d ''' % (
                dev_lat,
                dev_long,
                current_user.age - 4,
                current_user.age + 4,
                current_user.gender,
                current_user.language,
                total_users - len(suggestions))
        else:
            query = '''
                            SELECT evb_users.id
                            FROM evb_users
                            INNER JOIN evb_genre_ranks ON evb_genre_ranks.user_id = evb_users.id
                            LEFT JOIN evb_dialogs ON evb_dialogs.user_id = evb_users.id
                            WHERE age >=%d
                            AND age <%d
                            AND gender != %d
                            AND `language` = '%s'
                            AND (facebook_id IS NOT NULL OR vk_id IS NOT NULL)
                            AND evb_dialogs.id IS NULL
                            AND evb_users.id >= FLOOR(RAND()* (select max(id) from evb_users))
                            LIMIT 0,%d ''' % (
                current_user.age - 4,
                current_user.age + 4,
                current_user.gender,
                current_user.language,
                total_users - len(suggestions))
        log2console('Executing query: %s' % query)
        dbcur.execute(query)
        for person in dbcur.fetchall():
            id = person[0]
            if geo_provided:
                dist = person[1]
            else:
                dist = 0
            try:
                u = User.objects.get(id=id)
                suggestions.append({'user':u, 'distance':dist})
            except User.DoesNotExist:
                pass
        log2console("%s user(s) was appended" % len(suggestions))

    found_users = list(suggestions)
    log2console('%d real users found' % len(found_users))
    result_users = privileged_users + found_users
    shuffle(result_users)

    if in_test_case:
        return result_users

    log2console('Saving package')
    st = OfferSet(user_id=current_user.id, type=1, qty=len(result_users))
    st.save()
    for o in result_users:
        offer = Offer(oset_id=st.id, person_id=o['user'].id, distance = o['distance'])
        offer.save()
    log2console('New offer set for user %s has been saved' % current_user.name)
    dbcur.close()


@periodic_task(run_every=crontab(minute='*/1'))
def send_pushes():
    log2console('Running APNS queued task')
    c = Notification.objects.filter(delivered_at=None, retries__lt=3).count()
    if c < 1:
        log2console('Nothing to send')
    else:
        query = Notification.objects.filter(delivered_at=None, retries__lt=3).query
        log2console("SQL:%s" % query)
        notifications = Notification.objects.filter(delivered_at=None, retries__lt=3)
        for notification in notifications:
            if notification.recommendation is not None:
                song = notification.recommendation.song
                log2console('Finding associated song, if necessary')
                if song.enqueued_at is None and song.processed_at is None:
                    log2console('Song lookup required')
                    song.enqueued_at = datetime.now()
                    song.save(force_update=True)
                    get_song(song)
                    song.save(force_update=True)
                    song.processed_at = datetime.now()
            else:
                log2console('Notification has no song, no lookup required')
            args = json.loads(notification.push_args.encode('utf-8'))
            bundle = notification.device.client_bundle
            log2console('Current device bundle: %s' % bundle)
            if (bundle is  None) or ('adhook' not in str(bundle)):
                cert_path = APS_CERT_PATH_PRODUCTION
            else:
                cert_path = APS_CERT_ADHOOK_PRODUCTION

            log2console('Using certificate from file: %s' % cert_path)
            apple_service = APNs(use_sandbox=False, cert_file=cert_path)

            token = notification.device.token
            target = notification.device.user.id

            if 'loc_args' in args:
                log2console('Additional arguments provided')
                al = PayloadAlert('', loc_key=args['loc_key'], loc_args=args['loc_args'])
            elif 'loc_key' in args:
                log2console("PUSH LOC KEY: %s" % args['loc_key'])
                al = PayloadAlert('', loc_key=args['loc_key'])
            else:
                al = PayloadAlert(args['alert'])

            if 'quantity' in args:
                badge = args['quantity']
                log2console('Unread messages for current user: %d' % badge)
                if 'custom_data' in args:
                    payload = Payload(alert=al, sound='default', badge=badge, custom=args['custom_data'])
                else:
                    payload = Payload(alert=al, sound='default', badge=badge)
            else:
                payload = Payload(alert=al, sound='default')
            log2console('Sending notification for User Id = %d' % target)
            notification.retries += 1
            notification.save()

            if notification.recommendation is not None:
                rec = notification.recommendation
                rec.status = 2
                rec.save()
                log2console("Recommendations status %d changed to DELIVERED" % rec.id)
            try:
                log2console('Sending notification for device: %s' % token)
                apple_service.gateway_server.send_notification(token, payload)
                notification.delivered_at = datetime.now()
                notification.save(force_update=True)
            except BaseException, ex:
                log2console('Error while sending PUSH notification:%s' % ex.message)

        i = 0
        failed_items = apple_service.feedback_server.items()
        for f in failed_items:
            i += 1
        log2console("Failed: %d item(s) to send" % i)
        for (token_hex, fail_time) in failed_items:
            log2console('Unable to deliver PUSH message for: %s' % token_hex)
            try:
                dev = Device.objects.get(token=token_hex)
                try:
                    n = Notification.objects.get(device_id=dev.id)
                    n.delivered_at = None
                    n.save(force_update=True)
                except:
                    pass
            except Device.DoesNotExist:
                pass


def set_coords_to_bots():
    log2console("Setting random coordinates for all of the bot's")
    try:
        users = User.objects.filter(facebook_id=None, vk_id=None)
        seed()
        for u in users:
            lat_int = randint(54.0, 57.0)
            lng_int = randint(35.0, 41.0)
            dec =   1.0 / randint(1,100)
            lt = float(lat_int) + dec
            lng = float(lng_int) + dec
            try:
                dev = Device.objects.get(user_id=u.id)
            except Device.DoesNotExist:
                dev = Device(type=2, user_id=u.id)
            dev.lat = lt
            dev.long = lng
            dev.save()
            log2console('Device for %s with lat=%f and long=%f has been registered' % (u.id, lt, lng))
    except:
        log2console('Routine failed')
        return


def get_old_profiles():
    log2console('Downloading profiles from old serve...')
    log2console('Connecting server: easyvibe.cloudapp.net')
    try:
        ms = pymssql.connect(host='easyvibe.cloudapp.net', user='apiuser', password='Social1236', database='Easyvibe')
        cur = ms.cursor()
        log2console('Connected, cursor has been set')
        #1103
        #user_query = "select id,AccessToken,birthday,updatedAt,email,name,gender,vkid,fbid,vktoken,fbtoken,picture,lang" \
        #             "  FROM Easyvibe.dbo.Clients where id=1922"
        user_query = "select id,AccessToken,birthday,updatedAt,email,name,gender,vkid,fbid,vktoken,fbtoken,picture,lang  FROM Easyvibe.dbo.Clients where CreatedAt>'2014-01-04'"
        #user_query = "select id,AccessToken,birthday,updatedAt,email,name,gender,vkid,fbid,vktoken,fbtoken,picture,lang  FROM [Easyvibe].[dbo].[Clients] where fbid is NULL and vkid IS NULL"
        cur.execute(user_query)
        users = cur.fetchall()
        log2console('Total users: %d' % len(users))
        for user in users:
            log2console('User has been acquired, building an instance of User object: %d' % user[0])
            try:
                u = User.objects.get(access_token=str(user[1]).lower())
            except User.DoesNotExist:
                u = User(access_token=str(user[1]).lower(), birthday=user[2], updated_at=user[3], email=user[4],
                         name=user[5].encode('utf-8'), gender=user[6], vk_id=user[7], facebook_id=user[8],
                         vk_token=user[9], facebook_token=user[10])
                if user[12] is not None:
                    u.language = user[12].decode('cp1251').encode('utf-8')
                img_url = user[11]
                u.save(force_insert=True)
                if img_url is not None:
                    img_res = urlopen(img_url)
                    pic = img_res.read()
                    pic_url = save_avatar(u.id, pic)
                    u.pic = pic_url['big']
                    u.save(force_update=True)
                today = datetime.today()
                if u.birthday is not None:
                    str_birthday = str(u.birthday)
                    if str_birthday.find('.') > -1:
                        str_birthday = str_birthday[0:str_birthday.find('.')]
                    age = relativedelta(today, datetime.strptime(str_birthday, '%Y-%m-%d %H:%M:%S'))
                    u.age = age.years
                else:
                    u.age = 0

                u.save()
                log2console('User %s saved' % u.name)

            log2console('Fetching playlist')
            song_query = '''
              SELECT author,title,genre,Year
              FROM [Easyvibe].[dbo].[UsersInSongs] inner join Songs on [UsersInSongs].SongId = Songs.Id
              WHERE ClientId=%d and Author != '' and genre != ''
              ''' % user[0]
            cur.execute(song_query)
            songs = cur.fetchall()
            for song in songs:
                #log2console('Song has been acquired, building an instance of Song object')
                try:
                    song_hash = create_song_hash(song[0].decode('cp1251').encode('utf-8'),
                                                 song[1].decode('cp1251').encode('utf-8'))[:16]
                except UnicodeEncodeError:
                    song_hash = create_song_hash(song[0].encode('utf-8'), song[1].encode('utf-8'))[:16]
                try:
                    s = Song.objects.get(hash=song_hash)
                except Song.DoesNotExist:
                    s = Song(author=song[0].encode('utf-8'),
                             title=song[1].replace('$', 'S').encode('utf-8'), year=song[3])
                    try:
                        s.genre = song[2].replace('$', 'S').decode('cp1251').encode('utf-8')
                    except UnicodeEncodeError:
                        s.genre = song[2].replace('$', 'S').encode('utf-8')
                    s.hash = song_hash
                    s.save(force_insert=True)
                    log2console('Song %s - %s (%d) saved' % (s.author, s.title, s.year))
                u.songs.add(s)
            analyze_playlist(u.id)
        cur.close()
    except pymssql.ProgrammingError, err:
        return (False, err[1])





