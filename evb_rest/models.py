from django.db import models, connection
from datetime import datetime
from dateutil import relativedelta
from hashlib import md5
from base64 import b64encode
from time import mktime

from easyvibe.settings import VIBES_MAX_QTY


def create_song_hash(author, title):
    return md5(b64encode(author + title)).hexdigest()


SONG_GENRES = {
    1: 'Rock',
    2: 'Pop',
    3: 'Rap & Hip-Hop',
    4: 'Easy Listening',
    5: 'Dance & House',
    6: 'Instrumental',
    7: 'Metal',
    8: 'Dubstep',
    9: 'Jazz & Blues',
    10: 'Drum & Bass',
    11: 'Trance',
    12: 'Chanson',
    13: 'Ethnic',
    14: 'Acoustic & Vocal',
    15: 'Reggae',
    16: 'Classical',
    17: 'Indie Pop',
    19: 'Speech',
    21: 'Alternative',
    22: 'Electropop & Disco',
    18: 'Other'
}


# Create your models here.
class Album(models.Model):
    author = models.CharField(max_length=100, null=False, db_index=True)
    title = models.CharField(max_length=100, null=False, db_index=True)
    artwork = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        ordering = ('title',)
        db_table = 'evb_albums'


class Song(models.Model):
    author = models.CharField(max_length=255, null=False, db_index=True)
    genre = models.CharField(max_length=255, blank=True, null=True)
    title = models.CharField(max_length=255, null=False, db_index=True)
    year = models.SmallIntegerField(blank=True, null=True)
    artwork = models.CharField(max_length=255, blank=True, null=True)
    url = models.CharField(max_length=500, blank=True, null=True)
    itunes_url = models.CharField(max_length=500, blank=True, null=True)
    song_hash = models.CharField(max_length=64, blank=False, null=False, name='hash')
    vk_uid = models.CharField(max_length=128, blank=True, null=True)
    albums = models.ManyToManyField(Album, blank=True, null=True, related_name='contained_songs')
    enqueued_at = models.DateTimeField(blank=True, null=True)
    processed_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        ordering = ('title',)
        db_table = 'evb_songs'


class SongView(models.Model):
    author = models.CharField(max_length=100)
    genre = models.CharField(max_length=100)
    title = models.CharField(max_length=100)
    direction = models.BinaryField()
    created_at = models.DateTimeField()
    artwork = models.CharField(max_length=255, blank=True, null=True)
    url = models.CharField(max_length=500, blank=True, null=True)

    def get_created_at(self):
        return datetime.now()

    def get_direction(self):
        return 1

    class Meta:
        managed = False


class User(models.Model):
    id = models.AutoField(primary_key=True)
    access_token = models.CharField(max_length=128, unique=True)
    birthday = models.DateField(blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    is_developer = models.BooleanField(default=False)
    email = models.CharField(max_length=50, blank=True, null=True)
    facebook_id = models.CharField(max_length=128, blank=True, null=True)
    facebook_token = models.CharField(max_length=500, blank=True, null=True)
    gender = models.SmallIntegerField(default=0)
    name = models.CharField(max_length=128)
    age = models.SmallIntegerField(default=0)
    pic = models.CharField(max_length=255, blank=True, null=True)
    vk_id = models.IntegerField(blank=True, null=True)
    vk_token = models.CharField(max_length=255, blank=True, null=True)
    language = models.CharField(max_length=2, choices=(('ru', 'Russian'), ('us', 'US'),), default='ru')
    songs = models.ManyToManyField(Song, related_name='contained_users')
    vibes_qty = models.SmallIntegerField(default=VIBES_MAX_QTY)
    vibes_max_qty = models.SmallIntegerField(default=VIBES_MAX_QTY)
    vibe_used_at = models.DateTimeField(default=None,blank=None)
    current_step = models.IntegerField(default=1)
    step_init = models.BooleanField(default=False)
    step_init_at = models.DateTimeField(default=None,blank=None)
    recharge_timeout = models.TimeField(default='04:00')
    do_multivibe = models.BooleanField(default=False)
    multivibe_sent_at = models.DateTimeField(default=None, blank=None)
    do_invite = models.BooleanField(default=False)
    invite_sent_at = models.DateTimeField(default=None, blank=None)
    is_notified = models.BooleanField(default=False)
    is_deleted = models.BooleanField(default=False)


    def user_exists(self):
        if self.facebook_id is not None:
            fb_user_exist = len(User.objects.filter(facebook_id=self.facebook_id)) > 0
        else:
            fb_user_exist = False
        if self.vk_id is not None:
            vk_user_exist = len(User.objects.filter(vk_id=self.vk_id)) > 0
        else:
            vk_user_exist = False
        return fb_user_exist or vk_user_exist

    def count_age(self):
        today = datetime.today()
        if self.birthday is not None:
            age = relativedelta.relativedelta(today, datetime.strptime(str(self.birthday), '%Y-%m-%d'))
            return age.years
        else:
            return 0

    def vibe_counter_timestamp(self):
        return mktime(self.vibe_used_at.timetuple())

    class Meta:
        db_table = 'evb_users'

class Genre(models.Model):
    name  = models.CharField(max_length=128,unique=True)

    class Meta:
        db_table = "evb_genres"

class GenreRanks(models.Model):
    user = models.ForeignKey(User, related_name='current_ranks')
    genre = models.ForeignKey(Genre)
    rank = models.SmallIntegerField(default=0)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        unique_together = ("user","genre")
        db_table = "evb_genre_ranks"


class Device(models.Model):
    token = models.CharField(null=True, max_length=80)
    type = models.SmallIntegerField(int, default=1)
    last_send_push = models.DateTimeField(blank=True, null=True)
    locale = models.CharField(max_length=20, blank=True, null=True)
    client_version = models.CharField(max_length=10, null=True, blank=True)
    client_bundle = models.CharField(max_length=200, null=True, blank=True)
    long = models.DecimalField(blank=True, null=True, max_digits = 18, decimal_places=14)
    lat = models.DecimalField(blank=True, null=True, max_digits = 18, decimal_places=14 )
    placemark = models.CharField(max_length=200, blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    user = models.ForeignKey(User, unique=True, related_name='current_device')

    class Meta:
        db_table = "evb_devices"


class Dialog(models.Model):
    user = models.ForeignKey(User, related_name='owned_dialogs')
    companion = models.ForeignKey(User, related_name='guest_dialogs')
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    is_deleted = models.BooleanField(default=False)

    class Meta:
        db_table = "evb_dialogs"

class Transaction(models.Model):
    tx_no = models.CharField(max_length=128, unique=True)
    owner = models.ForeignKey(User,related_name='outgoing_transactions')
    user = models.ForeignKey(User,related_name='incoming_transactions')
    is_rejected = models.BooleanField(default=False)
    created_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        ordering = ['tx_no']
        db_table = "evb_user_transations"

class Recommendation(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    status = models.IntegerField(max_length=1, choices=((1, 'Pending'), (2, 'Delivered'), (3, 'Read'), (4, 'Failed')),
                                 default=1)
    in_inbox = models.BooleanField(default=False)
    creator = models.ForeignKey(User, unique=False, related_name='sent_recommendations')
    recipient = models.ForeignKey(User, unique=False, related_name='accepted_recommendations')
    song = models.ForeignKey(Song)
    dialog = models.ForeignKey(Dialog, unique=False, blank=True, related_name='set_recommendations')
    transaction = models.ForeignKey(Transaction, unique=True, blank=True, null=True, default=None)

    class Meta:
        db_table = "evb_recommendations"


class Notification(models.Model):
    device = models.ForeignKey(Device, related_name='set_notifications')
    push_args = models.CharField(max_length=500, blank=True, null=False)
    recommendation = models.ForeignKey(Recommendation, blank=True, null=True, related_name='set_notifications')
    created_at = models.DateTimeField(auto_now_add=True)
    delivered_at = models.DateTimeField(blank=True, null=True)
    retries = models.SmallIntegerField(default=0, null=False)

    class Meta:
        db_table = "evb_apns_notifications"


class Permission(models.Model):
    account_owner = models.ForeignKey(User, related_name='set_permissions')
    account_guest = models.ForeignKey(User)
    account_status = models.IntegerField(max_length=1, choices=((1, 'Approved'), (2, 'Rejected')), default=None)

    class Meta:
        db_table = "evb_permissions"

class Invitation(models.Model):
    user = models.ForeignKey(User,related_name='set_invitations')
    created_at = models.DateTimeField(auto_now_add=True)
    show_qty = models.IntegerField(default=0)
    used_at = models.DateTimeField(null=True,blank=True)

    class Meta:
        ordering = ['user']
        db_table = "evb_user_invitations"


class OfferSet(models.Model):
    user = models.ForeignKey(User, related_name='affected_packages')
    type = models.IntegerField(max_length=1,choices=((1,'Regular'),(2,'Award'), (3,'Extra')), default=1)
    qty = models.IntegerField(max_length=2)
    used_at = models.DateTimeField(null=True,blank=True)

    class Meta:
        ordering = ['user']
        db_table = 'evb_offer_set'


class Offer(models.Model):
    oset = models.ForeignKey(OfferSet, related_name='main_set')
    person = models.ForeignKey(User, related_name='involved_offers')
    distance = models.DecimalField(default=0,max_digits=8, decimal_places=4)
    used_at = models.DateTimeField(null=True,blank=True)


    class Meta:
        ordering = ['person']
        db_table = 'evb_offer'


class CalcDistance():
    '''
    Calling a stored routine to compute a distance between current and desired user
    '''
    @staticmethod
    def GetDistanceInKm(user_im, user_foreign):
        cursor = connection.cursor()
        mydev = user_im.current_device.get()
        foreign_dev = user_foreign.current_device.get()
        im_lat = mydev.lat
        im_long = mydev.long

        f_lat = foreign_dev.lat
        f_long = foreign_dev.long

        if im_lat is None or im_long is None or f_lat is None or f_long is None:
            return None

        query = "select round(CALCULATEDISTANCEKM(%f, %f, %f, %f)) as D" % (im_lat,im_long,f_lat,f_long)
        cursor.execute(query)
        ret_result = cursor.fetchone()[0]
        cursor.close()
        return ret_result

