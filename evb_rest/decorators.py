__author__ = 'Usachev'

from rest_framework.response import Response
from rest_framework import status
from models import User
from utils import log2console

def auth_required(func):

    def check_auth(self, request, pk=None):
        log2console('Access validation')
        token = request.META.get('X-AUTH-TOKEN')
        if token is None:
            log2console('Token not present')
            return Response(status=status.HTTP_400_BAD_REQUEST)
        else:
            log2console('Incoming token: ' + token)
            user = User.objects.get(access_token__iexact=token)
            if user is None:
                log2console('Token not found')
                return Response(status=status.HTTP_401_UNAUTHORIZED)
            else:
                log2console('Recognized as ' + user.name)
                return func(self,request,pk)

    return check_auth

