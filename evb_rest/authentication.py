import logging
from rest_framework import authentication
from rest_framework import exceptions

from models import User


logger = logging.getLogger('web_logger')
class EasyvibeAuthentication(authentication.BaseAuthentication):

    def authenticate(self, request):
        token = request.META.get('HTTP_X_AUTH_TOKEN')
        from_date_interval = request.META.get('HTTP_X_FROM_DATE')
        if token is None:
            logger.error('Token not present')
            raise exceptions.AuthenticationFailed
        else:
            logger.debug('Incoming token: ' + token)
            try:
                user = User.objects.get(access_token=token)
                #logger.info('Recognized as ' + user.name.encode('cp1251') + ' (id=%s)' % user.id)
                return (user,None)
            except User.DoesNotExist:
                logger.debug('Token not found')
                raise exceptions.AuthenticationFailed
