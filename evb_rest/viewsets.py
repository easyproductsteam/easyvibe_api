from rest_framework import viewsets, status
from rest_framework.parsers import JSONParser, FormParser
from rest_framework.response import Response
import logging

from evb_rest.tasks import request_offer
from serializers import *


__author__ = 'Usachev'

logger = logging.getLogger('web_logger')


class ItunesViewSet(viewsets.ViewSet):
    parser_classes = (FormParser, JSONParser)
    model = Song

    def create(self, request):
        logger.warning("Old profiles sync started")
        request_offer(1)
        return Response(status=status.HTTP_200_OK)