from django.db import connection
from django.db.models import Q
import itunes
import json
from os import path, makedirs,remove, walk
from random import randrange
from PIL import Image
import cStringIO as StringIO
import logging
import random
import string
from time import mktime,sleep
import urllib2

from easyvibe.settings import DOMAIN_HOME, STATIC_URL, SONG_ROOT, SOUNDCLOUD_CLIENT_ID, STATIC_ROOT
from evb_rest.models import Album, Song, Dialog, Recommendation, Device, Notification


__author__ = 'Usachev'

logger = logging.getLogger('web_logger')
from datetime import datetime


def log2console(text, eoln=True):
    n = datetime.now()
    if eoln:
        print '[%s] %s' % (n.strftime('%d/%b/%Y %H:%M:%S'), text.encode('utf-8'))
    else:
        print '[%s] %s' % (n.strftime('%d/%b/%Y %H:%M:%S'), text.encode('utf-8')),


def save_avatar(user_id, data):
    savepath = path.join(STATIC_ROOT, 'profiles', str(user_id))
    logger.info('Writing to: %s' % savepath)
    if not path.exists(savepath):
        logger.info('Directory %s does not exist' % savepath)
        makedirs(savepath)
    else:
        # clear all old files from profile directory
        for root, dirs, files in walk(savepath, topdown=False):
            if len(files) > 0:
                for name in files:
                    remove_path = path.join(savepath, name)
                    logger.info('Removing file: %s' % remove_path)
                    remove(remove_path)

    name_salt = randrange(1000)
    stream = StringIO.StringIO(data)
    im = Image.open(stream)
    w = im.size[0]
    h = im.size[1]

    if w > 200 or h > 200:
        basewidth = 200
        wpercent = (basewidth / float(w))
        hsize = int((float(h) * float(wpercent)))
        im.thumbnail((basewidth, hsize), Image.ANTIALIAS)
        crp = im.crop((0, 0, 200, 200))
    else:
        crp = im
    try:
        crp.save(savepath + '/profile_%d.jpg' % name_salt, 'JPEG', quality=80, optimize=True)
    except BaseException:
        crp.convert('RGB').save(savepath + '/profile_%d.jpg' % name_salt, 'JPEG', quality=80, optimize=True)

    if w > 200 or h > 200:
        crp.thumbnail((160, 160), Image.ANTIALIAS)
    try:
        crp.save(savepath + '/profile_%d_sm.jpg' % name_salt, "JPEG", quality=80, optimize=True)
    except BaseException:
        crp.convert('RGB').save(savepath + '/profile_%d_sm.jpg' % name_salt, "JPEG", quality=80, optimize=True)

    pic_url = DOMAIN_HOME + STATIC_URL + 'profiles/' + str(user_id) + '/profile_%d.jpg' % name_salt
    sm_pic_url = DOMAIN_HOME + STATIC_URL + 'profiles/' + str(user_id) + '/profile_%d_sm.jpg' % name_salt
    return {'big': pic_url, 'small': sm_pic_url}

def cut_mp3_file(filepath):
    from pydub import AudioSegment
    song = AudioSegment.from_mp3(filepath)
    seconds = 30 * 1000
    segment = song[:seconds]
    filename = randomword(10) + '.mp3'

    file = open(path.join(path.dirname(filepath), filename),'wb')
    #AudioSegment.ffmpeg = '/usr/bin/ffmpeg'

    segment.export(file,format="mp3")
    file.close()
    remove(filepath)
    return filename

def save_mp3_file(usock,savepath):
    f = open(savepath, 'wb')  #opening file for write and that too in binary mode.
    file_size = int(usock.info().getheaders("Content-Length")[0])  #getting size in bytes of file(pdf,mp3...)
    log2console("Downloading...%s bytes left" % (file_size))

    downloaded = 0
    block_size = 8192  #bytes to be downloaded in each loop till file pointer does not return eof
    while True:
        buff = usock.read(block_size)
        if not buff:  #file pointer reached the eof
            break
        downloaded = downloaded + len(buff)
        f.write(buff)
        #download_status = r"%3.2f%%" % (downloaded * 100.00 / file_size)  #Simple mathematics
        #download_status = download_status + (len(download_status) + 1) * chr(8)
        #print download_status, "done"
    f.close()
    log2console("Downloading done")


def analyze_playlist(user_id):
    from django.db import connection
    from math import floor

    logger = logging.getLogger('web_logger')
    logger.debug("Playlist analysis thread started")
    logger.info("Analysing playlist of id = %d" % user_id)

    dbcur = connection.cursor()
    query = '''SELECT (count(1))/(
SELECT count(1) FROM evb_users_songs WHERE user_id=%d)*100 as W,evb_genres.id
  FROM evb_users_songs
     INNER JOIN evb_songs ON evb_users_songs.song_id = evb_songs.id
     INNER JOIN evb_genres ON genre = evb_genres.name
     WHERE user_id=%d
group by id''' % (user_id,user_id)
    dbcur.execute(query)
    for r in dbcur.fetchall():
        rank = floor(float(r[0]))
        genre_id = int(r[1])
        logger.debug("Genre %s, rank: %s" % (genre_id,rank))
        try:
            dbcur.execute('Select count(1) FROM evb_genre_ranks WHERE user_id=%d AND genre_id=%d' % (user_id,genre_id))
            row_count = int(dbcur.fetchone()[0])
            if row_count > 0:
                logger.debug("Genre record exists, updating")
                dbcur.execute('UPDATE evb_genre_ranks SET rank=%d where user_id=%d and genre_id=%d' % (rank,user_id,genre_id))
            else:
                logger.debug("Genre record doesn't exists, creating a new one")
                dbcur.execute('INSERT INTO evb_genre_ranks(user_id,genre_id,rank) VALUES(%d,%d,%d)' % (user_id,genre_id,rank))
        except BaseException:
            pass
    logger.debug("Genre ranks saved")
    dbcur.close()

def randomword(length):
    return ''.join(random.choice(string.lowercase) for i in range(length))

def find_in_soundcloud(author,title):
    import soundcloud
    log2console('Looking in Soundcloud for %s - %s' % (author,title))
    log2console('Contacting Soundcloud...',eoln=False)
    client = soundcloud.Client(client_id=SOUNDCLOUD_CLIENT_ID)
    log2console('Soundcloud connected')

    log2console('Finding song: %s' % author + ' ' + title)
    tracks = client.get('/tracks', q=author)
    list_len = len(tracks.data)
    current_track = None
    for i in range(list_len):
        track = tracks.data[i].obj
        if str(track['title'].encode('utf-8')).lower().find(title.encode('utf-8').lower()) != -1:
            current_track = track
            break
    if current_track is None:
        log2console('Track was not found :(')
    else:
        log2console('Track found!')
        artwork=current_track[u'artwork_url']
        if current_track['streamable']:
            track_permalink = current_track['permalink_url']
            log2console('Permalink acquired: %s' % track_permalink)
            trk = client.get('/resolve',url=track_permalink)
            trk_url = trk.obj['permalink_url']
            log2console('Track url resolved: %s' % trk_url)
            log2console('Track is streamable, url: %s' % current_track['stream_url'])
            log2console('Acquiring stream')
            request = urllib2.Request(current_track['stream_url'] + '.json?client_id=' + SOUNDCLOUD_CLIENT_ID)
            request.get_method = lambda: 'HEAD'
            file_stream = urllib2.urlopen(request)
            return {'stream': file_stream, 'artwork': artwork, 'url': trk_url}


def get_song(s):
    log2console('Looking up for %s:%s' % (s.author,s.title))
    import urllib2,re
    s.url = None
    s.save(force_update = True)
    #for s in unresolved_songs:
    song_author = s.author.encode('utf-8')
    r = re.compile('([^\s!(0-9)\.-]+)')
    t_author = ''
    t_title = ''
    matches = r.findall(s.author)
    for m in matches:
        t_author = t_author + m + ' '

    t = s.title
    pos = s.title.find('(')
    if pos != -1:
        t = s.title[0:pos]
    else:
        pos = s.title.find('[')
        if pos != -1:
            t = s.title[0:pos]

    matches = r.findall(t)
    for m in matches:
        t_title = t_title + m + ' '

    t_title = t_title.strip()
    t_author = t_author.strip()
    log2console('Finding song %s:%s' % (t_author, t_title))
    artist = itunes.search_artist(t_author)
    current_track = None
    current_artwork = None
    if artist is not None and len(artist) > 0:
        log2console('Artist found')
        try:
            for album in artist[0].get_albums():
                for track in album.get_tracks():
                    track_name = track.get_name()
                    current_artwork = track.artwork['100'].replace('100', '450')
                    #log2console('Track: "%s"' % track_name)
                    try:
                        if track_name.lower() == t_title.lower():
                            current_track = track
                            break
                    except UnicodeDecodeError:
                        pass
        except itunes.ServiceException, e:
            log2console('iTunes API error: %s' % e._message)

    if current_track is not None:
        preview_url = current_track.get_preview_url()
        #log2console('Track found: %s' % preview_url)
        artwork = current_track.get_artwork()
        if len(artwork) > 0:
            #log2console('Cover found: %s' % artwork['100'].replace('100', '450'))
            #saving track data to DB
            song_album = current_track.album.name
            try:
                db_album = Album.objects.get(author=song_author, title=song_album)
            except Album.DoesNotExist:
                db_album = Album(author=song_author, title=song_album)
                db_album.save()
            db_album.contained_songs.add(s)
            db_album.artwork = artwork['100'].replace('100', '450')
            db_album.save()


            s.artwork = db_album.artwork
            log2console('Saving song sample locally')
            try:
                if not path.exists(path.join(SONG_ROOT, song_author.replace(' ', '_'))):
                    makedirs(path.join(SONG_ROOT, song_author.replace(' ', '_')))
                sample_path = path.join(SONG_ROOT, song_author.replace(' ', '_'),
                                        current_track.name.replace(' ', '_') + '.mp3')
                log2console('Sample savepath:%s ' % sample_path)
                file_data = urllib2.urlopen(current_track.get_preview_url())
                save_mp3_file(file_data, sample_path)
                log2console('File saved')
                s.url = (
                DOMAIN_HOME + STATIC_URL + 'songs/' + song_author.replace(' ', '_') + '/' + current_track.name.replace(
                    ' ', '_') + '.mp3').encode('utf-8')
                s.itunes_url = current_track.get_url()
                log2console('Itunes URL saved: %s' % s.itunes_url)
                s.save(force_update=True)
            except BaseException, e:
                log2console('Error:%s' % e.message)
                log2console('Unable to save sample, using direct link instead')
                s.url = current_track.get_preview_url()
            log2console('Sample shared url:%s' % s.url)
            s.processed_at = datetime.now()
            s.save(force_update=True)
            log2console('Song database updated with: %s - %s' % (song_author, current_track.name))
    else:
        #log2console('Track was not found, was looking for: "%s"' % t_title.encode('utf-8'))
        response = find_in_soundcloud(t_author, t_title)
        if response is not None:
            log2console('Track was found in Soundcloud!')
            if response['artwork'] is not None:
                log2console('Cover found: %s' % response['artwork'])
                s.artwork = response['artwork']

            log2console('Saving song sample locally')
            try:
                #from transliterate import translit
                folder_name = randomword(10)
                file_name = randomword(10)

                if not path.exists(path.join(SONG_ROOT, '_soundcloud')):
                    makedirs(path.join(SONG_ROOT, '_soundcloud'))
                sample_path = path.join(SONG_ROOT, '_soundcloud', file_name + '.mp3')
                log2console('Sample savepath:%s ' % sample_path)
                #file_data = urllib2.urlopen(current_track.get_preview_url())
                save_mp3_file(response['stream'], sample_path)
                log2console('File saved')
                cut_file_name = cut_mp3_file(sample_path)
                log2console('File duration cut of to first 30 sec.')
                song_url = DOMAIN_HOME + STATIC_URL + 'songs/' + '_soundcloud' + '/' + cut_file_name
                s.itunes_url = response['url']
                if s.artwork is None:
                    s.artwork = current_artwork
                s.url = song_url
                s.save()
            except BaseException, e:
                log2console('Error:%s' % e.message)
                log2console('Unable to save sample, using direct link instead')
                #s.url = current_track.get_preview_url()
            log2console('Sample shared url:%s' % s.url)
        else:
            log2console('Track was not found anywhere - song will be not playable')
            s.url = None
        s.processed_at = datetime.now()
        s.save(force_update=True)

    log2console('Song lookup completed')



def send_corp_vibe(recipient):
    '''
    Sends a vibe to new registered used
    '''

    logger.info('Waiting on for 3 min...')
    sleep(180)
    logger.debug('3 minutes was slept...')
    CORP_FB_VK_ID = 5979
    CORP_FB_ACC_ID = 5980

    # finding random song to send...
    song_totals = Song.objects.count()
    random_index = int(random.random()*song_totals)+1
    random_song = Song.objects.get(pk = random_index)

    if (recipient.vk_id is not None):
        sender_id = CORP_FB_VK_ID
    else:
        sender_id = CORP_FB_ACC_ID

    dbcur = connection.cursor()
    query = 'select count(1) from evb_recommendations where recipient_id=%d and dialog_id is NULL ' \
            'and status = 2' % recipient.id
    dbcur.execute(query)
    res = dbcur.fetchone()
    unread_out_of_dialog = int(res[0])

    query = 'select count(1) from evb_recommendations where recipient_id=%d and dialog_id is not NULL ' \
            'and status = 2' % recipient.id
    dbcur.execute(query)
    res = dbcur.fetchone()
    unread_in_dialog = int(res[0])

    try:
        user_dialog = Dialog.objects.get((Q(user_id=recipient.id, companion_id=sender_id) |
                                          Q(companion_id=sender_id, user_id=recipient.id)) &
                                         Q(is_deleted=0))
        unread_in_dialog += 1
        logger.info('Dialog with a target user exists')
    except Dialog.DoesNotExist:
        user_dialog = None
        unread_out_of_dialog += 1
        logger.info('Dialog with a target user does not exists')

    unread_qty = unread_in_dialog + unread_out_of_dialog
    r = Recommendation(creator_id=sender_id, recipient_id=recipient.id, song_id=random_song.id,
                       transaction_id=None)
    r.save()

    # creating a PUSH message
    push_args = {}
    push_args['loc_key'] = 'NEW_VIBE_KEY'
    push_args['quantity'] = unread_qty
    custom_data = {
        'created': mktime(r.created_at.timetuple()),
        'ci': unread_out_of_dialog,
        'cr': unread_in_dialog
    }

    if user_dialog is not None:
        r.dialog_id = user_dialog.id
        r.in_inbox = False
        custom_data['dialog_id'] = r.dialog_id
        user_dialog.updated_at = datetime.now()
        user_dialog.save(force_update=True)
    else:
        r.in_inbox = True

    r.status = 1
    r.save(force_update=True)
    logger.info('Recommendation for user %d have changed status to PENDING' % recipient.id)
    push_args['custom_data'] = custom_data

    user_device = Device.objects.filter(user_id=recipient.id).first()
    if user_device is not None:
        n = Notification(device_id=user_device.id, push_args=json.dumps(push_args),recommendation_id=r.id)
        n.save()
        logger.info('Notification for device %s queued' % user_device.id)
    else:
        logger.info('Device for user %s is not registered, unable to send notification' % recipient.id)



def create_sql_clause_list(items_list):
    sql_text = '('
    for i in items_list:
        sql_text += "'%s'," % i
        #sql_text = sql_text+("'%s'," % str(i))
    if len(sql_text) < 3:
        return '()'
    else:
        return sql_text[:-1] + ')'
