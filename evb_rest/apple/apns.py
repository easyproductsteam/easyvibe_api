import json

class ApnsException(BaseException):

    def __init__(self,error_text):
        self.message = error_text


class NotificationManager():

    def __init__(self,device_id,custom,loc_key=None):
        """
        Prepares an APNS message
        :param device_id: device id
        :param custom: dictionary of custom data
        :param loc_key: string for message localization
        """
        if custom is not None:
            self.custom_data = custom

        if device_id is not None:
            self.device_id = device_id
        else:
            raise ApnsException(error_text='for message delivery device id must be necessarily set')

        if loc_key is not None:
            if loc_key is dict:
                self.loc_key = loc_key
            else:
                raise ApnsException(error_text='custom data object must be a dictionary')

    def queue(self):
        from evb_rest.models import Notification
        push_args = {}
        if self.loc_key is not None:
            push_args['loc_key'] = self.loc_key
        if self.custom_data is not None:
            push_args['custom_data'] = self.custom_data
        n = Notification(device_id=self.device_id, push_args=json.dumps(push_args))
        n.save()

