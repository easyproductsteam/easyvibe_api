from django.db.models import Q
from rest_framework.decorators import *
import json
import logging
from rest_framework import viewsets, status
from rest_framework.parsers import JSONParser, FormParser
from rest_framework.response import Response
from django.db import connection
from time import mktime
from datetime import datetime

from evb_rest.authentication import EasyvibeAuthentication
from evb_rest.models import User, Dialog, Permission,Notification,Device
from evb_rest.serializers import UserSerializer


__author__ = 'Usachev'

logger = logging.getLogger('web_logger')

class UserViewSet(viewsets.ViewSet):
    model = User
    parser_classes = (JSONParser, FormParser,)
    authentication_classes = (EasyvibeAuthentication,)

    def retrieve(self, request, pk=None):
        logger.debug('Client connected')
        try:
            current_user = User.objects.get(id=pk)
            logger.info('User info fetch called by: %s for user %s' % (request.user.id, pk))
            serializer = UserSerializer(current_user)
            if current_user.age is not None:
                serializer.data['age'] = current_user.age
        except User.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def update(self, request, pk=None):
        try:
            logger.info('User info update called by id = %s for user id = %s' % (request.user.id, pk))
            name = request.DATA.get('user_name')
            email = request.DATA.get('email')
            age = request.DATA.get('age')
            lang = request.DATA.get('lang')
            gender = request.DATA.get('gender')

            old_user = User.objects.get(id=request.user.id)
            old_user.name = name or old_user.name
            old_user.email = email or old_user.email
            old_user.age = age or old_user.age

            if lang is not None:
                old_user.language = lang
            if gender is not None:
                old_user.gender = gender
            old_user.save(force_update=True)

            serializer = UserSerializer(old_user)
            if age is not None:
                serializer.data['age'] = age
            logger.debug('Data updated')
            return Response(serializer.data, status=status.HTTP_200_OK)
        except BaseException, e:
            return Response({'error': e.message}, status=status.HTTP_400_BAD_REQUEST)

    @action(methods=('POST',))
    def like(self, request, pk=None):
        logger.info('User like method called by id = %s' % request.user.id)
        try:
            logger.info('Dialog check query: %s' % Dialog.objects.filter(Q(user_id=request.user.id, companion_id=pk) | Q(user_id=pk,companion_id = request.user.id)).query)
            dialog = Dialog.objects.get(Q(user_id=request.user.id, companion_id=pk) | Q(user_id=pk,companion_id = request.user.id))
        except Dialog.DoesNotExist:
            dialog = Dialog(user_id=request.user.id, companion_id=pk)
            dialog.save()
        dbcur = connection.cursor()
        dbcur.execute('''UPDATE evb_recommendations SET dialog_id=%d,status=3,updated_at=NOW() WHERE creator_id=%d AND
        recipient_id=%d AND status = 2''' % (dialog.id, int(pk), request.user.id))

        # ci
        query = 'select count(1) from evb_recommendations where recipient_id=%d and dialog_id is NULL ' \
                'and status = 2' % int(pk)
        logger.info('Executing SQL: %s' % query)
        dbcur.execute(query)
        res = dbcur.fetchone()
        unread_out_of_dialog = int(res[0])

        # cr
        query = 'select count(1) from evb_recommendations where recipient_id=%d and dialog_id is not NULL ' \
                'and status = 2' % int(pk)
        logger.info('Executing SQL: %s' % query)
        dbcur.execute(query)
        res = dbcur.fetchone()
        unread_in_dialog = int(res[0])

        dbcur.close()
        already_exist = Permission.objects.filter(account_owner_id=request.user.id, account_guest_id=pk).count() > 0
        if not already_exist:
            p = Permission(account_owner_id=request.user.id, account_guest_id=pk, account_status=1)
            p.save()
            # sending PUSH, if necessary
            # creating a PUSH message
            push_args = {}
            push_args['loc_key'] = 'PROF_OPEN_KEY'
            push_args['loc_args'] = [request.user.name]
            push_args['quantity'] = unread_in_dialog + unread_out_of_dialog
            push_args['custom_data'] = {
                'created': mktime(datetime.now().timetuple()),
                'ci': unread_out_of_dialog,
                'cr': unread_in_dialog
            }
            user_device = Device.objects.filter(user_id=pk).first()
            if user_device is not None and user_device.token is not None:
                n = Notification(device_id=user_device.id, push_args=json.dumps(push_args))
                n.save()
                logger.info('Notification for device %s queued' % user_device.id)
            else:
                logger.error('Device for user %s is not registered, unable to send notification' % pk)
        return Response(status=status.HTTP_200_OK)


    @action(methods=('POST',))
    def dislike(self, request, pk=None):
        logger.info('User dislike method called by id = %s' % request.user.id)
        # some actions to prevent  user suggestion for account owner for some time
        dbcur = connection.cursor()
        dbcur.execute(
            'UPDATE evb_recommendations SET status=3,updated_at=NOW() WHERE creator_id=%d AND recipient_id=%d AND status=2' % (
                int(pk), request.user.id))
        dbcur.close()
        already_exist = Permission.objects.filter(account_owner_id=request.user.id, account_guest_id=pk).count() > 0
        if not already_exist:
            p = Permission(account_owner_id=request.user.id, account_guest_id=pk, account_status=2)
            p.save()
        # sending PUSH, if necessary
        return Response(status=status.HTTP_200_OK)
