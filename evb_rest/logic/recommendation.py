from datetime import datetime
import json
import logging
from rest_framework import viewsets, status
from rest_framework.parsers import JSONParser
from rest_framework.response import Response
from django.db import connection
from django.db.models import Max, Q
from time import mktime

from evb_rest.authentication import EasyvibeAuthentication
from evb_rest.models import Song, Recommendation, Permission, User, Dialog, Device, Notification, Transaction, OfferSet, \
    Offer
from evb_rest.serializers import UserSerializer, SongSerializer


__author__ = 'Usachev'

logger = logging.getLogger('web_logger')


class RecommendationViewSet(viewsets.ViewSet):
    parser_classes = (JSONParser,)
    authentication_classes = (EasyvibeAuthentication,)
    model = Recommendation

    def list(self, request):
        logger.info('Inbox get called by id = %s' % request.user.id)
        response = []
        # fetching a list of new incoming recommendations
        #people who send me a vibe
        dbcur = connection.cursor()
        dbcur.execute(
            'SELECT distinct creator_id FROM evb_recommendations WHERE recipient_id=%d and status=2 AND dialog_id IS NULL' % request.user.id)
        for person_id in dbcur.fetchall():
            sender = User.objects.get(id=person_id[0])
            sent_vibes = [s.song for s in sender.sent_recommendations.filter(recipient_id=request.user.id, status=2)]
            sent_times = [s.created_at for s in
                          sender.sent_recommendations.filter(recipient_id=request.user.id, status=2, dialog_id=None)]
            last_time = sender.sent_recommendations.filter(recipient_id=request.user.id, status=2,
                                                           dialog_id=None).aggregate(
                Max('created_at'))
            if last_time is not None:
                t = mktime(last_time['created_at__max'].timetuple())
            else:
                t = 0
            user_serializer = UserSerializer(sender)
            song_serializer = SongSerializer(sent_vibes)
            user_data = user_serializer.data
            song_data = song_serializer.data
            i = 0
            for d in song_data:
                d['key'] = mktime(sent_times[i].timetuple())
                i += 1
            q = Permission.objects.filter(account_owner=request.user.id, account_guest=person_id[0],
                                          account_status=1).count()
            user_data['profile_open'] = True if q > 0 else False
            response.append({'created_at': t, 'user': user_data, 'songs': song_data})
        dbcur.close()
        return Response(response, status=status.HTTP_200_OK)


    def create(self, request):
        logger.info('Recommendation suggestions method called by id = %s' % request.user.id)
        song_hash = request.DATA.get('song_hash')
        try:
            # song = Song.objects.get(hash=song_hash.encode('utf-8'))
            a_user = User(request.user)
            if a_user.vibe_used_at is not None:
                pass
            #logger.info('Finding suggestions for: %s ' % song.genre)
            suggestions = []
            privileged_users = []
            dbcur = connection.cursor()
            device = request.user.current_device.get()
            if request.user.is_developer:
                logger.info('Debug user!!!')
                suggestions = User.objects.filter(is_developer=1).exclude(id=request.user.id)
            else:
                # First of all,find Users who have used invitations
                logger.info('Finding Users who have used invitations')
                ranks = [r.genre_id for r in request.user.current_ranks.order_by('-rank').all()]
                dbcur.execute('''SELECT min( id ) , user_id, show_qty FROM evb_user_invitations
                WHERE used_at IS NULL GROUP BY user_id''')
                for inv in dbcur.fetchall():
                    row_id = int(inv[0])
                    user_id = int(inv[1])
                    show_qty = int(inv[2])
                    try:
                        user = User.objects.get(id=user_id)
                        if user.gender != request.user.gender:
                            privileged_users.append(user)
                            # decrease a counter of User's privileged showing
                            show_qty -= 1
                            if show_qty < 1:
                                dbcur.execute(
                                    '''UPDATE evb_user_invitations SET show_qty = %d, used_at = NOW() WHERE id=%d''' % (
                                    show_qty, row_id))
                            else:
                                dbcur.execute('''UPDATE evb_user_invitations SET show_qty = %d WHERE id=%d''' % (
                                show_qty, row_id))
                    except User.DoesNotExist:
                        pass
                logger.info('Privileged users found: %d' % len(privileged_users))
                if request.user.step_init == False:
                    logger.warning("Step is not initialized, initializing new one...")
                    logger.warning("Finding %d people" % request.user.vibes_max_qty)
                    total_bots = int(request.user.vibes_max_qty * 0.5)
                    total_users = int(request.user.vibes_max_qty - total_bots)
                    logger.warning("%d bot(s) will be shown" % total_bots)
                    logger.warning("%d real user(s) will be shown" % total_users)
                    query = '''
                    SELECT evb_users.id FROM evb_users
                    WHERE gender != %d
                    AND facebook_id IS NULL AND vk_id IS NULL order by rand() LIMIT %d''' % (
                    request.user.gender, total_bots)
                    dbcur.execute(query)
                    results = dbcur.fetchall()
                    for person in results:
                        id = person[0]
                        try:
                            u = User.objects.get(id=id)
                            suggestions.append(u)
                        except User.DoesNotExist:
                            pass
                    logger.info("%s bot(s) added" % len(suggestions))

                    # query = '''
                    # SELECT evb_users.id FROM evb_users
                    # WHERE gender != %d
                    # AND (facebook_id IS NOT NULL OR vk_id IS NOT NULL) AND is_deleted=0 order by rand() LIMIT %d''' % (request.user.gender,request.user.vibes_max_qty-q)
                    # dbcur.execute(query)
                    # results = dbcur.fetchall()
                    # for person in results:
                    #     id = person[0]
                    #     try:
                    #         u = User.objects.get(id=id)
                    #         suggestions.append(u)
                    #     except User.DoesNotExist:
                    #         pass
                    # logger.info("%s user(s) added" % len(results))

                    #looking for most recent genres for current user...
                    for r in ranks:
                        #for testers only !
                        query = '''
                        SELECT evb_users.id, evb_dialogs.id, name, evb_genre_ranks.genre_id, rank
                        FROM evb_users
                        INNER JOIN evb_genre_ranks ON evb_genre_ranks.user_id = evb_users.id
                        LEFT JOIN evb_dialogs ON evb_dialogs.user_id = evb_users.id
                        WHERE age >=%d
                        AND age <%d
                        AND gender != %d
                        AND genre_id =%d
                        AND evb_dialogs.id IS NULL
                        AND evb_users.id NOT IN (
                        select user_id FROM evb_user_transations WHERE owner_id=%d AND created_at > DATE_ADD(NOW(),INTERVAL -1 DAY)
                        )
                        ORDER BY rank DESC LIMIT 0,%d ''' % (
                        request.user.age - 4, request.user.age + 4, request.user.gender, r, request.user.id,
                        total_users + 1)

                        #logger.info('Executing SQL: %s' % query)
                        dbcur.execute(query)
                        for person in dbcur.fetchall():
                            id = person[0]
                            try:
                                u = User.objects.get(id=id)
                                suggestions.append(u)
                            except User.DoesNotExist:
                                pass
                        logger.info("%s user(s) found" % len(suggestions))
                        if len(suggestions) >= total_users:
                            break
                    request.user.step_init = True
                    request.user.step_init_at = datetime.now()
                    request.user.save(force_update=True)
                else:
                    logger.warning("Step initialized, current step is: %d" % request.user.current_step)
                    logger.warning("Vibes left: %d" % request.user.vibes_qty)
                    logger.warning("Finding %d people" % request.user.vibes_qty)

                    total_bots = int(request.user.vibes_qty * 0.5)
                    total_users = int(request.user.vibes_qty - total_bots)
                    logger.warning("%d bot(s) will be shown" % total_bots)
                    logger.warning("%d real users will be shown" % total_users)
                    query = '''
                    SELECT evb_users.id FROM evb_users
                    WHERE gender != %d
                    AND facebook_id IS NULL AND vk_id IS NULL order by rand() LIMIT %d''' % (
                    request.user.gender, total_bots)
                    dbcur.execute(query)
                    results = dbcur.fetchall()
                    for person in results:
                        id = person[0]
                        try:
                            u = User.objects.get(id=id)
                            suggestions.append(u)
                        except User.DoesNotExist:
                            pass
                    logger.info("%s bot(s) added" % len(suggestions))

                    # query = '''
                    # SELECT evb_users.id FROM evb_users
                    # WHERE gender != %d
                    # AND (facebook_id IS NOT NULL OR vk_id IS NOT NULL) AND is_deleted=0 order by rand() LIMIT %d''' % (request.user.gender,request.user.vibes_qty-q)
                    # dbcur.execute(query)
                    # results = dbcur.fetchall()
                    # for person in results:
                    #     id = person[0]
                    #     try:
                    #         u = User.objects.get(id=id)
                    #         suggestions.append(u)
                    #     except User.DoesNotExist:
                    #         pass
                    # logger.info("%s user(s) added" % len(results))

                    # looking for most recent genres for current user...
                    for r in ranks:
                        query = '''
                        SELECT evb_users.id,evb_dialogs.id, name, evb_genre_ranks.genre_id, rank
                        FROM evb_users
                        INNER JOIN evb_genre_ranks ON evb_genre_ranks.user_id = evb_users.id
                        LEFT JOIN evb_dialogs ON evb_dialogs.user_id = evb_users.id
                        WHERE age >=%d
                        AND age <%d
                        AND gender != %d
                        AND genre_id =%d
                        AND evb_dialogs.id IS NULL
                        AND evb_users.id NOT IN (
                        select user_id FROM evb_user_transations WHERE owner_id=%d AND created_at > DATE_ADD(NOW(),INTERVAL -1 DAY)
                        )
                        ORDER BY rank DESC LIMIT 0,%d ''' % (
                        request.user.age - 4, request.user.age + 4, request.user.gender, r, request.user.id,
                        total_users + 1)

                        #logger.info('Executing query: %s' % query)
                        dbcur.execute(query)
                        for person in dbcur.fetchall():
                            id = person[0]
                            try:
                                u = User.objects.get(id=id)
                                suggestions.append(u)
                            except User.DoesNotExist:
                                pass
                        logger.info("%s user(s) found" % len(suggestions))
                        if len(suggestions) >= total_users:
                            break

            dbcur.close()
            found_users = list(suggestions)
            if len(found_users) >= 15:
                suggestions = privileged_users[:3] + found_users
            elif len(found_users) >= 10:
                suggestions = privileged_users[:7] + found_users
            elif len(found_users) >= 5:
                suggestions = privileged_users[:10] + found_users
            else:
                suggestions = privileged_users + found_users

            #suggestions = privileged_users + list(suggestions)
            request.user.is_notified = False
            request.user.save()
            logger.info('PUSH notification flag for new packet key was updated')
            if request.user.step_init == False:
                user_serializer = UserSerializer(suggestions[0:request.user.vibes_max_qty])
            else:
                user_serializer = UserSerializer(suggestions[0:request.user.vibes_qty])
            return Response({'recommended': user_serializer.data})
        except Song.DoesNotExist:
            logger.error('Trying to get recommended persons with unknown song')
            return Response({'error': 'unknown song hash'}, status=status.HTTP_400_BAD_REQUEST)

    def partial_update(self, request, pk=None):
        logger.info('Recommendation status update method called by id = %s' % request.user.id)
        try:
            rec = Recommendation.objects.get(id=int(pk))
            # determine additional request data
            if request.DATA.get('fe_flag') is not None:
                rec.in_inbox = True
                logger.info('Flurry event flag changed status to FALSE')
            else:
                rec.status = 3
            rec.save(force_update=True)
            return Response(status=status.HTTP_200_OK)
        except Recommendation.DoesNotExist:
            return Response({'error': 'recommendation not found'}, status=status.HTTP_404_NOT_FOUND)


    def update(self, request, pk=None):
        logger.info('Transaction package processing called by id = %s' % request.user.id)
        logger.info('Acquiring transaction list...')

        tx_records = request.DATA.get('transactions')
        if tx_records is None:
            return Response({'error': 'transactions list cannot be empty'}, status=status.HTTP_400_BAD_REQUEST)
        logger.debug('%d transaction(s) acquired' % len(tx_records))

        has_dialog = False
        processed_tx = []
        try:
            for tx in tx_records:
                # check the transaction for being already processed
                is_rejected = False
                tx_cnt = Transaction.objects.filter(tx_no=tx[u'tx_id']).count()
                if tx_cnt > 0:
                    processed_tx.append(tx[u'tx_id'])
                    logger.debug("Transaction was already processed")
                    continue
                elif tx[u'user'] is None or tx[u'tx_id'] is None or tx[u'rejected'] is None:
                    logger.error("Bad transaction")
                    continue
                else:
                    if tx[u'rejected']:
                        logger.debug('ID %s rejects ID %s, decreasing Vibe counter' % (request.user.id, tx[u'user']))
                        # decreasing vibe counter
                        request.user.vibes_qty -= 1
                        if request.user.vibes_qty < 0:
                            request.user.vibes_qty = 0
                        request.user.vibe_used_at = datetime.now()
                        request.user.save()
                        #creating transaction record
                        tran = Transaction(tx_no=tx[u'tx_id'], is_rejected=True, user_id=tx[u'user'],
                                           owner_id=request.user.id)
                        tran.save()
                    else:
                        # user's statistical info
                        current_user = User.objects.get(id=request.user.id)
                        dbcur = connection.cursor()
                        query = 'select count(1) from evb_recommendations where recipient_id=%d and dialog_id is NULL ' \
                                'and status = 2' % tx[u'user']
                        logger.debug('Executing SQL: %s' % query)
                        dbcur.execute(query)
                        res = dbcur.fetchone()
                        unread_out_of_dialog = int(res[0])

                        query = 'select count(1) from evb_recommendations where recipient_id=%d and dialog_id is not NULL ' \
                                'and status = 2' % tx[u'user']
                        logger.debug('Executing SQL: %s' % query)
                        dbcur.execute(query)
                        res = dbcur.fetchone()
                        unread_in_dialog = int(res[0])

                        logger.debug('Check whatever sender and recipient dialog exists')
                        try:
                            user_dialog = Dialog.objects.get((Q(user_id=tx[u'user'], companion_id=request.user.id) |
                                                              Q(companion_id=tx[u'user'], user_id=request.user.id)) &
                                                             Q(is_deleted=0))
                            has_dialog = True
                            unread_in_dialog += 1
                            logger.debug('Dialog with a target user exists')
                        except Dialog.DoesNotExist:
                            user_dialog = None
                            has_dialog = False
                            unread_out_of_dialog += 1
                            logger.debug('Dialog with a target user does not exists')

                        unread_qty = unread_in_dialog + unread_out_of_dialog
                        logger.debug('Check qty of user available vibes')
                        if request.user.vibes_qty == 0:
                            logger.debug(
                                "Checks whether the dialog between current user and his companion is being open or not.")
                            if not has_dialog:
                                logger.debug('User has no vibes to send')
                                continue
                            else:
                                logger.debug("Open dialog(s) found, vibe delivery allowed")

                        # creating a recommendation to target user
                        try:
                            song = Song.objects.get(hash=tx[u'song_hash'])
                        except Song.DoesNotExist:
                            logger.warning('Song with hash=%s does not exists' % tx[u'song_hash'])
                            continue

                        #creating a transaction record
                        tran = Transaction(tx_no=tx[u'tx_id'], is_rejected=False, user_id=tx[u'user'],
                                           owner_id=request.user.id)
                        tran.save()

                        r = Recommendation(creator_id=request.user.id, recipient_id=tx[u'user'], song_id=song.id,
                                           transaction_id=tran.id)
                        r.save()

                        # creating a PUSH message
                        push_args = {}
                        push_args['loc_key'] = 'NEW_VIBE_KEY'
                        push_args['quantity'] = unread_qty
                        custom_data = {
                            'created': mktime(r.created_at.timetuple()),
                            'ci': unread_out_of_dialog,
                            'cr': unread_in_dialog
                        }

                        if user_dialog is not None:
                            r.dialog_id = user_dialog.id
                            r.in_inbox = False
                            custom_data['dialog_id'] = r.dialog_id
                            user_dialog.updated_at = datetime.now()
                            user_dialog.save(force_update=True)
                        else:
                            r.in_inbox = True
                            # decreasing vibe counter
                            request.user.vibes_qty -= 1
                            if request.user.vibes_qty < 0:
                                request.user.vibes_qty = 0
                            request.user.vibe_used_at = datetime.now()
                            request.user.save()

                        r.status = 1
                        r.save(force_update=True)
                        logger.info('Recommendation for user %d have changed status to PENDING' % tx[u'user'])
                        push_args['custom_data'] = custom_data

                        user_device = Device.objects.filter(user_id=tx[u'user']).first()
                        if user_device is not None:
                            n = Notification(device_id=user_device.id, push_args=json.dumps(push_args),
                                             recommendation_id=r.id)
                            n.save()
                            logger.info('Notification for device %s queued' % user_device.id)
                        else:
                            logger.error('Device for user %s is'
                                         ' not registered, unable to send notification' % tx[u'user'])

                            # finding current OfferSet for incoming user
                    sets = OfferSet.objects.filter(user_id=request.user.id, used_at__isnull=True).order_by('id')
                    if len(sets) > 0:
                        logger.debug('Desired package: %d' % sets[0].id)
                        try:
                            person = Offer.objects.get(oset_id=sets[0].id, person_id=tx[u'user'], used_at__isnull=True)
                            person.used_at = datetime.now()
                            person.save(force_update=True)
                            offer_cnt = Offer.objects.filter(oset_id=sets[0].id, used_at__isnull=True).count()
                            if offer_cnt == 0:
                                sets[0].used_at = datetime.now()
                                sets[0].save(force_update=True)
                        except Offer.DoesNotExist:
                            pass
                    processed_tx.append(tx[u'tx_id'])
                    logger.debug('Transaction saved as processed')
        except BaseException, ex:
            logger.error('SQL error: %s' % ex.message)
            # return Response({'error': 'transaction package processing failure'}, status=status.HTTP_400_BAD_REQUEST)
        return Response({'transactions': processed_tx}, status=status.HTTP_200_OK)