from datetime import datetime, timedelta
from django.db import connection
from django.utils import timezone
import json
import logging
from rest_framework import viewsets, status
from rest_framework.decorators import *
from rest_framework.parsers import JSONParser, FormParser
from rest_framework.response import Response
from time import mktime

from easyvibe.settings import VIBES_MAX_QTY, STEP1_INTERVAL, STEP2_INTERVAL, STEP3_INTERVAL, STEP4_INTERVAL
from evb_rest.authentication import EasyvibeAuthentication
from evb_rest.models import User, Song, Recommendation, Dialog, Device, Notification, create_song_hash


__author__ = 'Usachev'

logger = logging.getLogger('web_logger')

class VibeViewSet(viewsets.ViewSet):
    authentication_classes = (EasyvibeAuthentication,)
    parser_classes = (FormParser,)
    model = User

    @action(methods=('POST',))
    def invite(self, request, pk=None):
        logger.info('Mark invite as send method has been called by id = %s' % request.user.id)
        if request.user.do_invite:
            try:
                request.user.vibes_qty = 25
                request.user.vibes_max_qty = 25
                request.user.do_invite = False
                request.user.invite_sent_at = datetime.now()
                request.user.save(force_update=True)
                return Response(status=status.HTTP_200_OK)
            except BaseException, e:
                return Response({'error':e.message},status=status.HTTP_400_BAD_REQUEST)
        else:
            return  Response(status=status.HTTP_304_NOT_MODIFIED)


    @action(methods=('POST',))
    def set_counter(self, request, pk=None):
        #logger.info('Alter vibe counter method has been called by id = %s' % request.user.id)
        value = int(pk)
        do_multivibe = request.user.do_multivibe
        do_invite = request.user.do_invite
        tz_aware_time = timezone.now()

        if request.user.vibe_used_at != None:
            t_delta = tz_aware_time - request.user.vibe_used_at
            logger.info('User Id = %d -> last vibe sent at: %s, T-delta: %s min.' % (request.user.id,request.user.vibe_used_at,t_delta.seconds/60))
            #x_delta = timedelta(hours=request.user.recharge_timeout.hour)
            x_delta = timedelta(minutes=request.user.recharge_timeout.minute)
            if t_delta > x_delta and request.user.vibes_qty == 0:
                if request.user.current_step == 0:
                    request.user.vibes_max_qty = VIBES_MAX_QTY * 0.4
                    request.user.current_step = 1
                    request.user.do_invite = False
                    request.user.do_multivibe = False
                    request.user.invite_sent_at = None
                    request.user.multivibe_sent_at = None
                    request.user.recharge_timeout = STEP1_INTERVAL
                elif request.user.current_step == 1:
                    request.user.vibes_max_qty = VIBES_MAX_QTY * 0.25
                    request.user.current_step = 2
                    request.user.recharge_timeout = STEP2_INTERVAL
                elif request.user.current_step == 2:
                    request.user.vibes_max_qty = VIBES_MAX_QTY * 0.25
                    request.user.current_step = 3
                    request.user.do_multivibe = True
                    request.user.recharge_timeout = STEP3_INTERVAL
                elif request.user.current_step == 3:
                    request.user.vibes_max_qty = VIBES_MAX_QTY * 0.1
                    request.user.current_step = 4
                    request.user.do_invite = True
                    request.user.do_multivibe = False
                    request.user.recharge_timeout = STEP4_INTERVAL
                elif request.user.current_step == 4:
                    request.user.current_step = 0
                    request.user.do_invite = False
                    request.user.do_multivibe = False
                    request.user.vibes_max_qty = 0
                    request.user.recharge_timeout = STEP4_INTERVAL

                request.user.step_init = False
                request.user.step_init_at = None
                request.user.is_notified = False
                request.user.vibes_qty = request.user.vibes_max_qty
        else:
            #do the same as Step 1, if User is new
            request.user.vibes_max_qty = VIBES_MAX_QTY * 0.4
            request.user.current_step = 1
            request.user.do_invite = False
            request.user.do_multivibe = False
            request.user.recharge_timeout = STEP1_INTERVAL

        if value != 0:
            request.user.vibe_used_at = datetime.now()
        request.user.vibes_qty += value
        if request.user.vibes_qty > request.user.vibes_max_qty:
            request.user.vibes_qty = request.user.vibes_max_qty
        if request.user.vibes_qty < 0:
            request.user.vibes_qty = 0


        #todo: optimize database access queries
        request.user.save()
        return Response({'vibes_total': request.user.vibes_max_qty,
                         'vibes_left': request.user.vibes_qty,
                         'step': request.user.current_step,
                         'multivibe': do_multivibe,
                         'invite': do_invite}, status=status.HTTP_200_OK)


class MultivibeViewSet(viewsets.ViewSet):
    authentication_classes = (EasyvibeAuthentication,)
    parser_classes = (FormParser, JSONParser)
    model = User

    @action(methods=('POST',))
    def skip(self, request, pk=None):
        try:
            logger.info("Skip multivibe method called")
            request.user.do_multivibe = False
            request.user.save(force_update=True)
            logger.info("Multivibe for user ID=%d has been cancelled" % request.user.id)
            return Response(status=status.HTTP_200_OK)
        except Exception, e:
            logger.error('SQL Exception: %s' % e.message)
            return Response({'error':e.message},status=status.HTTP_400_BAD_REQUEST)

    def create(self,request,pk=None):
        try:
            logger.info('Multivibe method called by ID=%d' % request.user.id)
            if request.user.do_multivibe == True:
                song_hash = request.DATA.get('song_hash')
                song_author = request.DATA.get('song_author')
                song_title = request.DATA.get('song_title')
                if song_hash is None:
                    return Response({'error':'Song hash is not present'},status=status.HTTP_400_BAD_REQUEST)
                try:
                    song = Song.objects.get(hash=song_hash)
                except Song.DoesNotExist:
                    if (song_author is not None) and (song_title is not None):
                        author = song_author.encode('utf-8')
                        title = song_title.encode('utf-8')
                        genre = 'Other'
                        song_hash = create_song_hash(author, title)
                        song = Song(author=author, genre=genre, title=title, hash=song_hash)
                        song.save()
                    else:
                        logger.warning('Song with hash=%s does not exists' % song_hash)
                        return Response({'error':'Song not found'},status=status.HTTP_204_NO_CONTENT)


                dbcur = connection.cursor()
                # user's statistical info
                current_user = User.objects.get(id=request.user.id)
                dbcur = connection.cursor()
                query = 'select count(1) from evb_recommendations where (creator_id = %d or recipient_id=%d) and status IN (1,2)' % (
                    current_user.id, current_user.id)
                logger.info('Executing SQL: %s' % query)
                dbcur.execute(query)
                res = dbcur.fetchone()
                unread_qty = res[0]

                query = 'select count(1) from evb_recommendations where (creator_id = %d or recipient_id=%d) and dialog_id is NULL  and status IN (1,2)' % (
                    current_user.id, current_user.id)
                logger.info('Executing SQL: %s' % query)
                dbcur.execute(query)
                res = dbcur.fetchone()
                unread_out_of_dialog = res[0]

                query = 'select count(1) from evb_recommendations where (creator_id = %d or recipient_id=%d) and dialog_id is not NULL  and status IN (1,2)' % (
                    current_user.id, current_user.id)
                logger.info('Executing SQL: %s' % query)
                dbcur.execute(query)
                res = dbcur.fetchone()
                unread_in_dialog = res[0]

                #finding users for multivibe
                query='''
                        SELECT evb_users.id
                        FROM evb_users
                        WHERE age >=%d
                        AND age <%d
                        AND gender != %d
                        ORDER by  Rand() LIMIT 0,25''' % (request.user.age -4,request.user.age+4,request.user.gender)
                logger.info("SQL query: %s" % query)
                dbcur.execute(query)
                for u in dbcur.fetchall():
                    try:
                        chosen_user = User.objects.get(id=u[0])
                        logger.info("Sending multivibe to %s (%d)" % (chosen_user.name,chosen_user.id))
                        r = Recommendation(creator_id=request.user.id, recipient_id=chosen_user.id, song_id=song.id)
                        # check whatever sender and recipient dialog exists
                        try:
                            user_dialog = Dialog.objects.get(user_id=chosen_user.id, companion_id=request.user.id)
                            r.dialog_id = user_dialog.id
                            r.status = 2
                        except Dialog.DoesNotExist:
                            pass
                        r.save()

                        push_args = {}
                        push_args['loc_key'] = 'NEW_VIBE_KEY'
                        push_args['quantity'] = unread_qty
                        custom_data = {
                            'created': mktime(r.created_at.timetuple()),
                            'companion_id': request.user.id,
                            'ci': unread_out_of_dialog,
                            'cr': unread_in_dialog
                        }

                        if r.dialog_id is not None:
                            custom_data['dialog_id'] = r.dialog_id
                        push_args['custom_data'] = custom_data

                        user_device = Device.objects.filter(user_id=chosen_user.id).first()
                        if user_device is not None and user_device.token is not None:
                            n = Notification(device_id=user_device.id, push_args=json.dumps(push_args))
                            n.save()
                            logger.info('Notification for device %s queued' % user_device.id)
                        else:
                            logger.error('Device for user %s is not registered, unable to send notification' % chosen_user.id)
                    except User.DoesNotExist:
                        pass
                dbcur.close()
                logger.info("Multivibe has been sent!")
                request.user.multivibe_sent_at = datetime.now()
                request.user.do_multivibe = False
                request.user.save()
                return Response(status=status.HTTP_200_OK)
            else:
                return Response({'error':'Multivibe flag was not set'},status=status.HTTP_406_NOT_ACCEPTABLE)
        except Exception, e:
            logger.error("Can't send multivibe: %s" % e.message)
            return Response({'error':e.message},status=status.HTTP_400_BAD_REQUEST)

