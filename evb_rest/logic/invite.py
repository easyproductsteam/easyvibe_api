from datetime import datetime
import logging
from rest_framework import viewsets, status
from rest_framework.parsers import FormParser
from rest_framework.response import Response

from easyvibe.settings import INVITE_SHOW_BONUS
from evb_rest.authentication import EasyvibeAuthentication
from evb_rest.models import Invitation


__author__ = 'Usachev'

logger = logging.getLogger('web_logger')



class InviteViewSet(viewsets.ViewSet):
    authentication_classes = (EasyvibeAuthentication,)
    parser_classes = (FormParser,)
    model = Invitation

    def create(self, request):
        logger.info('Invite method called by ID=%d' % request.user.id)
        try:
            inv = Invitation(user_id=request.user.id, show_qty=INVITE_SHOW_BONUS)
            inv.save()
            request.user.invite_sent_at = datetime.now()
            request.user.do_invite = False
            #request.user.recharge_timeout = ZERO_INTERVAL
            request.user.save()
            logger.info('Invitation has been saved')
            return Response(status=status.HTTP_200_OK)
        except BaseException, e:
            logger.error("Error: %s" % e.message)
            return Response({'error': e.message}, status=status.HTTP_400_BAD_REQUEST)


