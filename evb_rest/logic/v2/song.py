import logging
import threading
from rest_framework import viewsets, status
from rest_framework.parsers import JSONParser, FormParser
from rest_framework.response import Response

from evb_rest.authentication import EasyvibeAuthentication
from evb_rest.models import Song, create_song_hash, SONG_GENRES, Album, Genre
from evb_rest.utils import analyze_playlist


__author__ = 'Usachev'
logger = logging.getLogger('web_logger')

class SongViewSetV2(viewsets.ViewSet):
    model = Song
    parser_classes = (JSONParser, FormParser,)
    authentication_classes = (EasyvibeAuthentication,)


    def create(self, request):
        try:
            logger.info('Playlist upload called by id = %s' % request.user.id)
            hs = []
            current_user = request.user
            for song in request.DATA['songs']:
                author = song['author'].encode('utf-8')
                title = song['title'].encode('utf-8')
                song_hash = create_song_hash(author, title)[:16]
                try:
                    s = Song.objects.get(hash=song_hash)
                except Song.DoesNotExist:
                    genre_no = song['genre']
                    if genre_no in SONG_GENRES:
                        genre_name = SONG_GENRES[genre_no]
                    else:
                        if 'genre' in song.keys():
                            genre_name = song['genre'].encode('utf-8')
                        else:
                            genre_name = 'Other'
                    # add a kind of genre record
                    genre_qty = Genre.objects.filter(name=genre_name).count()
                    if genre_qty == 0:
                        genre = Genre(name=genre_name)
                        genre.save()
                        logger.debug("Genre record added for %s" % genre_name)

                    s = Song(author=song['author'].encode('utf-8'), genre=genre_name,
                             title=song['title'].encode('utf-8'), hash=song_hash)
                    s.save()
                    if u'url' in song.keys():
                        s.url = song['url'].encode('utf-8')
                    if u'uid' in song.keys():
                        s.vk_uid = song['uid'].encode('utf-8')
                        s.save()
                    if u'album' in song.keys():
                        try:
                            alb = Album.objects.get(author=author, title=song['album'].encode('utf-8'))
                        except Album.DoesNotExist:
                            alb = Album(author=author, title=song['album'].encode('utf-8'))
                            alb.save()
                        s.albums.add(alb)
                        if song['year'] is not None:
                            s.year = song['year']
                            #s.save()
                finally:
                    s.save()
                    current_user.songs.add(s)
                    hs.append(song_hash)
            logger.debug("%s song(s) was updated" % len(hs))
            #starting Playlist scan background task...
            t = threading.Thread(target=analyze_playlist,args=(request.user.id,))
            t.start()
            logger.debug("New thread started")
            return Response({'song_hash': hs}, status=status.HTTP_200_OK)
        except BaseException, e:
            return Response({'error': e.message}, status=status.HTTP_400_BAD_REQUEST)

    def partial_update(self, request, pk=None):
        try:
            logger.info("Playlist check called by id = %s" % request.user.id)
            if 'hashes' in request.DATA:
                missing = []
                playlist_ids = [song.id for song in request.user.songs.all()]
                for h in request.DATA['hashes']:
                    song = Song.objects.filter(hash=h.encode('utf-8')).first()
                    if song is None:
                        missing.append(h)
                    else:
                        song_id = Song.objects.get(hash=h).id
                        if song_id not in playlist_ids:
                            missing.append(h)
                return Response({'missing': missing}, status=status.HTTP_200_OK)
            else:
                return Response({'error': 'hashes array was not present'}, status=status.HTTP_400_BAD_REQUEST)
        except BaseException, e:
            return Response({'error': e.message}, status=status.HTTP_400_BAD_REQUEST)
