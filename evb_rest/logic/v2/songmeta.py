from datetime import datetime
import logging
from rest_framework import viewsets, status
from rest_framework.parsers import JSONParser, FormParser
from rest_framework.response import Response

from evb_rest.authentication import EasyvibeAuthentication
from evb_rest.models import Song


__author__ = 'Usachev'

logger = logging.getLogger('web_logger')

class SongMetaViewSetV2(viewsets.ViewSet):
    model = Song
    parser_classes = (JSONParser, FormParser,)
    authentication_classes = (EasyvibeAuthentication,)

    def update(self, request, pk=None):
        logger.info('Song enqueue method called by id = %s' % request.user.id)
        try:
            ret_data = []
            if 'hashes' in request.DATA:
                for h in request.DATA['hashes']:
                    pass
                    try:
                        song = Song.objects.get(hash=h.encode('utf-8'))
                        if song.processed_at is not None:
                            ret_data.append({song.hash: {'url': song.url if song.url is not None else '',
                                                         'cover': song.artwork if song.artwork is not None else ''}})
                        elif song.enqueued_at is None:
                            song.enqueued_at = datetime.now()
                            song.save(force_update=True)
                    except Song.DoesNotExist:
                        pass
                if len(ret_data) > 0:
                    return Response({'songmeta': ret_data}, status=status.HTTP_200_OK)
                else:
                    return Response(status.HTTP_200_OK)
            else:
                return Response({'error': 'hash list is not present or empty'}, status=status.HTTP_400_BAD_REQUEST)
        except BaseException, e:
            return Response({'error': e.message}, status=status.HTTP_400_BAD_REQUEST)

    def create(self, request):
        logger.info('Song meta method called by id = %s' % request.user.id)
        try:
            ret_data = []
            if 'hashes' in request.DATA:
                for h in request.DATA['hashes']:
                    try:
                        song = Song.objects.get(hash=h.encode('utf-8'))
                        if song.processed_at is not None:
                            ret_data.append({song.hash: {'url': song.url if song.url is not None else '',
                                                         'cover': song.artwork if song.artwork is not None else ''}})
                        else:
                            logger.debug('Metadata search was not initialized for song: %s:%s' % (song.author.encode('utf-8'),song.title.encode('utf-8')))
                            #ret_data.append({song.hash: {'url': '', 'cover': ''}})
                    except Song.DoesNotExist:
                        pass
                if len(ret_data) > 0:
                    return Response({'songmeta': ret_data}, status=status.HTTP_200_OK)
                else:
                    logger.debug('Metadata not found for song')
                    return Response({'error': 'metadata not found'}, status.HTTP_404_NOT_FOUND)
            else:
                return Response({'error': 'hash list is not present or empty'}, status=status.HTTP_400_BAD_REQUEST)
        except BaseException, e:
            return Response({'error': e.message}, status=status.HTTP_400_BAD_REQUEST)
