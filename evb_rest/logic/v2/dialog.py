from datetime import datetime
from django.db.models import Q
import logging
from rest_framework import viewsets, status
from rest_framework.parsers import JSONParser
from rest_framework.response import Response
from time import mktime

from evb_rest.authentication import EasyvibeAuthentication
from evb_rest.models import Song, User, Dialog, Recommendation
from evb_rest.serializers import UserSerializer, SongSerializer


__author__ = 'Usachev'

logger = logging.getLogger('web_logger')

class DialogViewSetV2(viewsets.ViewSet):
    authentication_classes = (EasyvibeAuthentication,)
    parser_classes = (JSONParser,)
    model = Song


    def list(self, request):
        logger.info('Dialog list method called by id = %s' % request.user.id)
        from_date = request.environ.get('HTTP_X_FROM_DATE')
        if from_date is not None:
            from_datetime = datetime.fromtimestamp(int(from_date))
            logger.warning('SQL: %s ' % Dialog.objects.filter(Q(is_deleted=0,user_id=request.user.id,updated_at__gte=from_datetime) |
            Q(is_deleted=0,companion_id=request.user.id,updated_at__gte=from_datetime)).order_by('-updated_at').query)
            owned_d = Dialog.objects.filter(Q(is_deleted=0,user_id=request.user.id,updated_at__gte=from_datetime) |
            Q(is_deleted=0,companion_id=request.user.id,updated_at__gte=from_datetime)).order_by('-updated_at')
        else:
            owned_d = Dialog.objects.filter(Q(is_deleted=0,user_id=request.user.id) | Q(is_deleted=0,companion_id=request.user.id)).order_by('-updated_at')
        dialogs = []
        for d in owned_d:
            if d.companion_id == request.user.id:
                user_key = d.user_id
            else:
                user_key = d.companion_id
            user = User.objects.get(pk=user_key)
            #logger.debug(Recommendation.objects.filter(Q(dialog_id=d.id, status=2)).exclude(creator_id=request.user.id).query)
            read_vibes_count = Recommendation.objects.filter(Q(dialog_id=d.id, status=2)).exclude(creator_id=request.user.id).count()

            dialog_unread = read_vibes_count > 0
            dialog_id = d.id
            serializer = UserSerializer(user)
            dialog = {'dialog_id': dialog_id, 'created_at': mktime(d.updated_at.timetuple()), 'unread': dialog_unread,
                      'user': serializer.data}
            dialogs.append(dialog)
        if len(dialogs) > 0:
            return Response({'dialogs': dialogs}, status=status.HTTP_200_OK)
        else:
            return Response(status=status.HTTP_404_NOT_FOUND)

    def retrieve(self, request, pk=None):
        logger.info('Dialog details method called by id = %s' % request.user.id)
        logger.info('Fetching dialog with id = %s' % pk)
        from_date = request.environ.get('HTTP_X_FROM_DATE')

        try:
            dialog_id = Dialog.objects.get(id=pk).id
            #from_datetime = None
            #dbcur = connection.cursor()
            if from_date is not None:
                query = '''SELECT id, created_at, updated_at, status, creator_id, recipient_id, song_id, dialog_id, in_inbox, transaction_id
  FROM easyvibe_db.evb_recommendations WHERE dialog_id=%d AND created_at>=from_unixtime('%s') AND id NOT IN
  (SELECT id FROM evb_recommendations WHERE dialog_id=%d AND evb_recommendations.status=1
  AND evb_recommendations.creator_id!=%d)''' % (dialog_id, from_date, dialog_id, request.user.id)
            else:
                query = '''SELECT id, created_at, updated_at, status, creator_id, recipient_id, song_id, dialog_id, in_inbox, transaction_id
  FROM easyvibe_db.evb_recommendations WHERE dialog_id=%d AND id NOT IN
  (SELECT id FROM evb_recommendations WHERE dialog_id=%d AND evb_recommendations.status=1
  AND evb_recommendations.creator_id!=%d)''' % (dialog_id, dialog_id, request.user.id)

            logger.info('Retrieving a list of vibes for dialog Id = %d' % (dialog_id))
            accepted_vibes = Recommendation.objects.raw(query)
            vibes = []
            for v in accepted_vibes:
                song = Song.objects.get(pk=v.song_id)
                serializer = SongSerializer(song)
                tstamp = v.created_at.timetuple()
                direction = 0 if v.creator_id == request.user.id else 1
                unread = v.status != 3

                tx_no = v.transaction.tx_no if v.transaction is not None else ''
                vibe = {'id': v.id, 'created_at': mktime(tstamp), 'direction': direction, 'unread': unread,'status':v.status,'fe_flag':v.in_inbox,
                        'tx_id':tx_no, 'song': serializer.data}
                vibes.append(vibe)
            if len(vibes) > 0:
                return Response({'recommendations': vibes }, status=status.HTTP_200_OK)
            else:
                Dialog.objects.raw('DELETE FROM evb_dialogs WHERE id=%d' % dialog_id)
                return Response({'error':"Dialog is empty"}, status=status.HTTP_406_NOT_ACCEPTABLE)
        except Dialog.DoesNotExist:
            return Response({'error': 'dialog not found'}, status=status.HTTP_404_NOT_FOUND)
