import base64
import logging
from rest_framework import viewsets, status
from rest_framework.parsers import JSONParser, FormParser
from rest_framework.response import Response

from evb_rest.authentication import EasyvibeAuthentication
from evb_rest.models import User
from evb_rest.utils import save_avatar


__author__ = 'Usachev'

logger = logging.getLogger('web_logger')

class PictureViewSetV2(viewsets.ViewSet):
    authentication_classes = (EasyvibeAuthentication,)
    parser_classes = (FormParser, JSONParser)
    model = User

    def update(self, request, pk):
        logger.info("Picture update called by id = %s" % request.user.id)
        if 'picture' not in request.DATA.keys():
            return Response({'error': 'Incorrect JSON'}, status=status.HTTP_400_BAD_REQUEST)
        else:
            pic_base64 = request.DATA.get('picture')
            try:
                pic_data = base64.decodestring(pic_base64)
                pic_url = save_avatar(request.user.id, pic_data)
                request.user.pic = pic_url['big']
                request.user.save()
                return Response({'picture': pic_url['big']}, status=status.HTTP_200_OK)
            except BaseException, e:
                return Response({'error': e.message}, status=status.HTTP_400_BAD_REQUEST)
