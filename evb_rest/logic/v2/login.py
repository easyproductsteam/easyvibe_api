from datetime import datetime, timedelta
import hashlib
import logging
import threading
from rest_framework import viewsets, status
from rest_framework.parsers import JSONParser, FormParser
from rest_framework.response import Response
from urllib2 import urlopen
from vkontakte import VKError

from easyvibe.settings import VIBES_MAX_QTY, APP_STORE_URL
from evb_rest.models import User, Device
from evb_rest.serializers import UserSerializer
from evb_rest.utils import save_avatar, log2console, send_corp_vibe


__author__ = 'Usachev'

logger = logging.getLogger('web_logger')

class LoginViewSetV2(viewsets.ViewSet):
    model = User
    parser_classes = (JSONParser, FormParser,)

    def create(self, request, pk=None):
        token = request.DATA.get('fb_token') or request.DATA.get('vk_token')
        device_id = request.DATA.get('device_id')
        vk_id = request.DATA.get('vk_id')
        lang = request.DATA.get('lang') or 'ru'
        bundle = request.DATA.get('bundle_id')

        if token is None:
            return Response(status=status.HTTP_400_BAD_REQUEST)
        if vk_id is not None:
            try:
                # acquire user profile's fields
                first_name = request.DATA.get('first_name').encode('utf-8')
                last_name = request.DATA.get('last_name').encode('utf-8')
                bdate = request.DATA.get('bdate')
                sex = request.DATA.get('sex')
                img_url = request.DATA.get('photo_max')

                # analyze them
                if sex == 2:
                    gender = 0
                else:
                    gender = 1
                access_token = hashlib.sha224(first_name + last_name + str(vk_id)).hexdigest()
                logger.info('VK authenticated: %s' % first_name + ' ' + last_name)
                #if 'bdate' is set..
                if bdate is not None and len(bdate) > 0:
                    logger.info('Bdate is: %s' % bdate)
                    try:
                        birthday = datetime.strptime(bdate, '%d.%m.%Y')
                    except ValueError:
                        logger.info('Birth date is not recognized, using default 19 years old age instead')
                        t = timedelta(days=6935)  # approx. 19 years up to now
                        birthday = datetime.now() - t
                else:
                    t = timedelta(days=6935)
                    birthday = datetime.now() - t

                if img_url is not None and len(img_url) > 0:
                    img_res = urlopen(img_url)
                else:
                    img_res = None
                #creating User object
                u = User(name=first_name + ' ' + last_name, vk_id=vk_id, vk_token=token, access_token=access_token,
                         gender=gender, language=lang)
                if birthday is not None:
                    u.birthday = birthday.strftime('%Y-%m-%d')

                serializer = UserSerializer(u)
                old_user = User.objects.filter(vk_id=vk_id).first()
            except VKError, e:
                if 'captcha_img' in e.error:
                    return Response({'captcha_url': e.error['captcha_img'], 'sid': e.error['captcha_sid']},
                                    status=status.HTTP_401_UNAUTHORIZED)
                else:
                    return Response({'error': e.error['error_msg']}, status=status.HTTP_401_UNAUTHORIZED)
        else:
            try:
                # acquire user profile's fields
                first_name = request.DATA.get('first_name').encode('utf-8')
                last_name = request.DATA.get('last_name').encode('utf-8')
                email = request.DATA.get('email')
                bdate = request.DATA.get('bdate')
                sex = request.DATA.get('sex')
                img_url = request.DATA.get('photo_max')
                fb_id = request.DATA.get('fb_id')

                name = '%s %s' % (first_name, last_name)
                access_token = hashlib.sha224(name + str(fb_id)).hexdigest()

                if email is not None:
                    logger.info('FB authenticated: %s (%s)' % (name.decode('utf-8'), email))
                else:
                    logger.info('FB authenticated: %s' % name)

                if sex == 'male':
                    gender = 0
                else:
                    gender = 1

                if bdate is not None and len(bdate) > 0:
                    logger.info('Bdate is: %s' % bdate)
                    try:
                        birthday = datetime.strptime(bdate, '%m/%d/%Y')
                    except ValueError:
                        logger.info('Birth date is not recognized, using default 19 years old age instead')
                        t = timedelta(days=6935)  # approx. 19 years up to now
                        birthday = datetime.now() - t
                else:
                    t = timedelta(days=6935)
                    birthday = datetime.now() - t

                img_res = urlopen(img_url)
                u = User(name=name,
                         email=email,
                         birthday=birthday.strftime('%Y-%m-%d'),
                         facebook_id=fb_id,
                         facebook_token=token,
                         access_token=access_token,
                         gender=gender,
                         language=lang)
                serializer = UserSerializer(u)
                old_user = User.objects.filter(facebook_id=fb_id).first()
            except BaseException as ex:
                log2console('Error: %s' % ex)
                return Response({'error': ex.message}, status=status.HTTP_401_UNAUTHORIZED)

        pic = img_res.read()
        #saving user account
        if old_user is not None:
            logger.info("User account found, updating user %d" % old_user.id)
            if u.email is not None:
                old_user.email = u.email

            pic_url = save_avatar(old_user.id, pic)
            old_user.pic = pic_url['big']
            old_user.access_token = access_token
            old_user.name = u.name
            old_user.birthday = u.birthday
            old_user.age = serializer.data['age']
            serializer.data['id'] = old_user.id
            serializer.data['picture'] = old_user.pic
            old_user.save()
            logger.info('Account updated')
        else:
            #set some user variables as default
            u.current_step = 1
            u.step_init_at = None
            u.step_init = False
            u.vibes_max_qty = VIBES_MAX_QTY * 0.4
            u.age = serializer.data['age']
            logger.debug('User initialized on Step: %d' % u.current_step)
            logger.debug('Max vibes qty is set to: %d' % u.vibes_max_qty)
            #save user object
            u.save()

            pic_url = save_avatar(u.id, pic)
            u.pic = pic_url['big']
            u.save()
            serializer.data['picture'] = pic_url['big']
            serializer.data['id'] = u.id
            logger.info('Account saved with ID=%d' % u.id)

        serializer.data['app_url'] = APP_STORE_URL
        #saving or updating device entry
        user_id = u.id or old_user.id
        old_device = Device.objects.filter(user_id=user_id).first()
        if old_device is None:
            logger.info("User's device was not found, creating a new one")
            if token is not None:
                logger.info('Setting up token: %s' % device_id)
            device = Device(type=1, token=device_id, user_id=user_id)
            device.client_bundle = 'com.astro.easyvibe'
            if bundle is not None:
                device.client_bundle = bundle
                logger.info('bundle data saved: %s' % device.client_bundle)
            device.save()
            logger.info('Device saved')
        else:
            logger.info('Old user device found, updading token: ' + device_id)
            old_device.token = device_id
            old_device.client_bundle = 'com.astro.easyvibe'
            if bundle is not None:
                old_device.client_bundle = bundle
                logger.info('bundle data updated: %s' % old_device.client_bundle)
            old_device.save()
            logger.info('Device updated')

        if u.id is not None:
            t = threading.Thread(target=send_corp_vibe, args=(User.objects.get(id=user_id),))
            t.start()
            logger.info("Background thread started")
        else:
            logger.info("User is not new")
        return Response({'auth_token': u.access_token, 'user': serializer.data}, status=status.HTTP_200_OK)