from datetime import datetime, timedelta
import json
import logging
from rest_framework import viewsets, status
from rest_framework.parsers import JSONParser
from rest_framework.response import Response
from django.db import connection
from django.db.models import Max, Q
from time import mktime
from django.utils import timezone

from evb_rest.authentication import EasyvibeAuthentication
from evb_rest.models import Song, Recommendation, Permission, User, Dialog, Device, Notification, Transaction, OfferSet, \
    Offer, create_song_hash
from evb_rest.serializers import UserSerializer, SongSerializer
from evb_rest.tasks import request_offer


__author__ = 'Usachev'

logger = logging.getLogger('web_logger')


class RecommendationViewSetV2(viewsets.ViewSet):
    parser_classes = (JSONParser,)
    authentication_classes = (EasyvibeAuthentication,)
    model = Recommendation

    def list(self, request):
        logger.info('Inbox get called by id = %s' % request.user.id)
        response = []
        #fetching a list of new incoming recommendations
        #people who send me a vibe
        dbcur = connection.cursor()
        dbcur.execute(
            'SELECT distinct creator_id FROM evb_recommendations WHERE recipient_id=%d and status=2 AND dialog_id IS NULL' % request.user.id)
        for person_id in dbcur.fetchall():
            sender = User.objects.get(id=person_id[0])
            sent_vibes = [s.song for s in sender.sent_recommendations.filter(recipient_id=request.user.id, status=2)]
            sent_times = [s.created_at for s in
                          sender.sent_recommendations.filter(recipient_id=request.user.id, status=2, dialog_id=None)]
            last_time = sender.sent_recommendations.filter(recipient_id=request.user.id, status=2,
                                                           dialog_id=None).aggregate(
                Max('created_at'))
            if last_time is not None:
                t = mktime(last_time['created_at__max'].timetuple())
            else:
                t = 0
            user_serializer = UserSerializer(sender)
            song_serializer = SongSerializer(sent_vibes)
            user_data = user_serializer.data
            song_data = song_serializer.data
            i = 0
            for d in song_data:
                d['key'] = mktime(sent_times[i].timetuple())
                i += 1
            q = Permission.objects.filter(account_owner=request.user.id, account_guest=person_id[0],
                                          account_status=1).count()
            user_data['profile_open'] = True if q > 0 else False
            response.append({'created_at': t, 'user': user_data, 'songs': song_data})
        dbcur.close()
        return Response(response, status=status.HTTP_200_OK)


    def create(self, request):
        #default variables
        do_multivibe = request.user.do_multivibe
        do_invite = request.user.do_invite
        lng = request.DATA.get('longitude')
        lat = request.DATA.get('latitude')
        placemark = request.DATA.get('placemark')

        if (lng is not None) and (lat is not None) and (placemark is not None):
            logger.warning('Geolocation provided: LAT:%f\tLNG:%f\tPLACEMARK:%s' % (lat, lng, placemark))
            devMan = request.user.current_device
            dev = devMan.get(user_id=request.user.id)
            dev.lat = lat
            dev.long = lng
            dev.placemark = placemark
            dev.save()

        current_step = request.user.current_step
        total_vibes = 0
        left_vibes = 0
        suggestions = []

        #looking for people
        has_people = bool(OfferSet.objects.filter(user_id=request.user.id, used_at__isnull=True).count() > 0)

        logger.info('Recommendation suggestions method called by id = %s' % request.user.id)
        tz_aware_time = timezone.now()

        if request.user.vibe_used_at is None:
            t_delta = None
        else:
            t_delta = tz_aware_time - request.user.vibe_used_at
            #if not has_people:
            #    request_offer(request.user.id)

        if has_people:
            # if there are people unused, calculate package size and quantity of unused vibes
            last_package = OfferSet.objects.filter(user_id=request.user.id, used_at__isnull=True).order_by('id')[0]
            total_vibes = Offer.objects.filter(oset_id=last_package.id).count()
            suggestions = [(o.person, o.distance) for o in
                           Offer.objects.filter(oset_id=last_package.id, used_at__isnull=True).order_by('distance')]
            #shuffle(suggestions)
            left_vibes = len(suggestions)

            #calculating delta:
            x_delta = timedelta(minutes=request.user.recharge_timeout.minute)
            if t_delta is not None:
                logger.info('User Id = %d -> last vibe sent at: %s, T-delta: %s min.' % (
                    request.user.id, request.user.vibe_used_at, t_delta.seconds / 60))
                if t_delta > x_delta:
                    request.user.vibe_used_at = datetime.now()
                    logger.info('Timeout is over, drawing invite and multivibe...')
                    if not do_multivibe:
                        logger.info('Special flags are not set, drawing a dice...')

                        from random import randrange  # throwing a dice

                        dice_value = randrange(1, 20)
                        logger.warning('Throwing a dice: %d' % dice_value)

                        if dice_value > 15:
                            #calculating delta:
                            if request.user.multivibe_sent_at is not None:
                                t_delta = tz_aware_time - request.user.multivibe_sent_at
                                x_delta = timedelta(minutes=180)
                                if t_delta > x_delta:
                                    do_multivibe = True
                                    do_invite = False
                                else:
                                    do_multivibe = False
                                    do_invite = False
                            else:
                                do_multivibe = True
                                do_invite = False
                            request.user.multivibe_sent_at = tz_aware_time
                        else:
                            do_multivibe = False
                            do_invite = True

                    request.user.is_notified = False
                    request.user.current_step = current_step
                else:
                    do_invite = True
        else:
            if t_delta is None:
                logger.info('User is new, creating new package of people')
                request_offer(request.user.id)
                last_package = OfferSet.objects.filter(user_id=request.user.id, used_at__isnull=True).order_by('id')[0]
                total_vibes = Offer.objects.filter(oset_id=last_package.id).count()
                suggestions = [(o.person, o.distance) for o in
                           Offer.objects.filter(oset_id=last_package.id, used_at__isnull=True).order_by('distance')]

        request.user.do_invite = do_invite
        request.user.do_multivibe = do_multivibe
        request.user.updated_at = datetime.now()
        request.user.save(force_update=True)

        user_serializer = UserSerializer([s[0] for s in suggestions])
        d = user_serializer.data
        i = 0
        for item in d:
            if suggestions[i][1] is None:
                item['dist'] = 0
            else:
                item['dist'] = int(suggestions[i][1])
            i += 1

        return Response({'recommended': user_serializer.data,
                         'max_vibes': total_vibes,
                         'left_vibes': left_vibes,
                         'multivibe': do_multivibe,
                         'invite': do_invite}, status=status.HTTP_200_OK)


    def partial_update(self, request, pk=None):
        logger.info('Recommendation status update method called by id = %s' % request.user.id)
        logger.debug('Acquiring transaction list...')

        tx_records = request.DATA.get('transactions')
        if tx_records is None:
            return Response({'error': 'transactions list cannot be empty'}, status=status.HTTP_400_BAD_REQUEST)
        logger.debug('%d transaction(s) acquired' % len(tx_records))
        processed_tx = []
        for tx in tx_records:
            #check the transaction for being already processed
            tx_cnt = Transaction.objects.filter(tx_no=tx[u'tx_id']).count()
            if tx_cnt > 0:
                processed_tx.append(tx[u'tx_id'])
                logger.debug("Transaction was already processed")
                continue
            elif tx[u'rec_id'] is None:
                logger.error("Bad transaction")
                continue
            else:
                try:
                    rec = Recommendation.objects.get(id=int(tx[u'rec_id']))
                    # determine additional request data
                    if u'fe_flag' in tx.keys():
                        rec.in_inbox = True
                        logger.info('Flurry event flag changed status to FALSE')
                    else:
                        rec.status = 3
                    rec.save(force_update=True)
                    processed_tx.append(tx[u'tx_id'])
                except Recommendation.DoesNotExist:
                    continue
        return Response({'transactions': processed_tx}, status=status.HTTP_200_OK)


    def update(self, request, pk=None):
        logger.info('Transaction package processing called by id = %s' % request.user.id)
        logger.debug('Acquiring transaction list...')

        tx_records = request.DATA.get('transactions')
        if tx_records is None:
            return Response({'error': 'transactions list cannot be empty'}, status=status.HTTP_400_BAD_REQUEST)
        logger.debug('%d transaction(s) acquired' % len(tx_records))

        has_dialog = False
        processed_tx = []
        try:
            for tx in tx_records:
                #check the transaction for being already processed
                is_rejected = False
                tx_cnt = Transaction.objects.filter(tx_no=tx[u'tx_id']).count()
                if tx_cnt > 0:
                    processed_tx.append(tx[u'tx_id'])
                    logger.debug("Transaction was already processed")
                    continue
                elif tx[u'user'] is None or tx[u'tx_id'] is None or tx[u'rejected'] is None:
                    logger.error("Bad transaction")
                    continue
                else:
                    if tx[u'rejected']:
                        logger.debug('ID %s rejects ID %s, decreasing Vibe counter' % (request.user.id, tx[u'user']))
                        # decreasing vibe counter
                        request.user.vibes_qty -= 1
                        if request.user.vibes_qty < 0:
                            request.user.vibes_qty = 0
                        request.user.vibe_used_at = datetime.now()
                        request.user.save()
                        #creating transaction record
                        tran = Transaction(tx_no=tx[u'tx_id'], is_rejected=True, user_id=tx[u'user'],
                                           owner_id=request.user.id)
                        tran.save()
                    else:
                        # user's statistical info
                        current_user = User.objects.get(id=request.user.id)
                        dbcur = connection.cursor()
                        query = 'select count(1) from evb_recommendations where recipient_id=%d and dialog_id is NULL ' \
                                'and status = 2' % tx[u'user']
                        logger.info('Executing SQL: %s' % query)
                        dbcur.execute(query)
                        res = dbcur.fetchone()
                        unread_out_of_dialog = int(res[0])

                        query = 'select count(1) from evb_recommendations where recipient_id=%d and dialog_id is not NULL ' \
                                'and status = 2' % tx[u'user']
                        logger.info('Executing SQL: %s' % query)
                        dbcur.execute(query)
                        res = dbcur.fetchone()
                        unread_in_dialog = int(res[0])

                        logger.info('Check whatever sender and recipient dialog exists')
                        try:
                            user_dialog = Dialog.objects.get((Q(user_id=tx[u'user'], companion_id=request.user.id) |
                                                              Q(companion_id=tx[u'user'], user_id=request.user.id)) &
                                                             Q(is_deleted=0))
                            has_dialog = True
                            unread_in_dialog += 1
                            logger.info('Dialog with a target user exists')
                        except Dialog.DoesNotExist:
                            user_dialog = None
                            has_dialog = False
                            unread_out_of_dialog += 1
                            logger.info('Dialog with a target user does not exists')

                        unread_qty = unread_in_dialog + unread_out_of_dialog
                        logger.info('Check qty of user available vibes')
                        # if request.user.vibes_qty == 0:
                        #     logger.info("Checks whether the dialog between current user and his companion is being open or not.")
                        #     if not has_dialog:
                        #         logger.warning('User has no vibes to send')
                        #         continue
                        #     else:
                        #         logger.info("Open dialog(s) found, vibe delivery allowed")

                        # creating a recommendation to target user
                        try:
                            song = Song.objects.get(hash=tx[u'song_hash'])
                        except Song.DoesNotExist:
                            if (u'song_author' in tx.keys()) and (u'song_title' in tx.keys()):
                                logger.warning('Song with hash=%s does not exists' % tx[u'song_hash'])
                                author = tx[u'song_author'].encode('utf-8')
                                title = tx[u'song_title'].encode('utf-8')
                                genre = 'Other'
                                song_hash = create_song_hash(author, title)
                                song = Song(author=author, genre=genre, title=title, hash=song_hash)
                                song.save()
                            else:
                                continue

                        #creating transaction record
                        tran = Transaction(tx_no=tx[u'tx_id'], is_rejected=False, user_id=tx[u'user'],
                                           owner_id=request.user.id)
                        tran.save()

                        r = Recommendation(creator_id=request.user.id, recipient_id=tx[u'user'], song_id=song.id,
                                           transaction_id=tran.id)
                        r.save()

                        # creating a PUSH message
                        push_args = {}
                        push_args['loc_key'] = 'NEW_VIBE_KEY'
                        push_args['quantity'] = unread_qty
                        custom_data = {
                            'created': mktime(r.created_at.timetuple()),
                            'ci': unread_out_of_dialog,
                            'cr': unread_in_dialog
                        }

                        if user_dialog is not None:
                            r.dialog_id = user_dialog.id
                            r.in_inbox = False
                            custom_data['dialog_id'] = r.dialog_id
                            user_dialog.updated_at = datetime.now()
                            user_dialog.save(force_update=True)
                        else:
                            r.in_inbox = True
                            # decreasing vibe counter
                            request.user.vibes_qty -= 1
                            if request.user.vibes_qty < 0:
                                request.user.vibes_qty = 0
                            request.user.vibe_used_at = datetime.now()
                            request.user.save()

                        r.status = 1
                        r.save(force_update=True)
                        logger.info('Recommendation for user %d have changed status to PENDING' % tx[u'user'])
                        push_args['custom_data'] = custom_data

                        user_device = Device.objects.filter(user_id=tx[u'user']).first()
                        if user_device is not None:
                            n = Notification(device_id=user_device.id, push_args=json.dumps(push_args),
                                             recommendation_id=r.id)
                            n.save()
                            logger.info('Notification for device %s queued' % user_device.id)
                        else:
                            logger.error('Device for user %s is'
                                         ' not registered, unable to send notification' % tx[u'user'])

                    # finding current OfferSet for incoming user
                    sets = OfferSet.objects.filter(user_id=request.user.id, used_at__isnull=True).order_by('id')
                    if len(sets) > 0:
                        logger.debug('Desired package: %d' % sets[0].id)
                        person = Offer.objects.filter(oset_id=sets[0].id, person_id=tx[u'user'], used_at__isnull=True)[
                            0]
                        if person is not None:
                            person.used_at = datetime.now()
                            person.save(force_update=True)
                            offer_cnt = Offer.objects.filter(oset_id=sets[0].id, used_at__isnull=True).count()
                            if offer_cnt == 0:
                                sets[0].used_at = datetime.now()
                                sets[0].save(force_update=True)
                        else:
                            pass
                    processed_tx.append(tx[u'tx_id'])
                    logger.debug('Transaction saved as processed')
        except BaseException, ex:
            logger.info('SQL error: %s' % ex.message)
            #return Response({'error': 'transaction package processing failure'}, status=status.HTTP_400_BAD_REQUEST)
        return Response({'transactions': processed_tx}, status=status.HTTP_200_OK)
