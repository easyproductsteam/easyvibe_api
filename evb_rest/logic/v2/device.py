from django.db import connection
import logging
from rest_framework import viewsets, status
from rest_framework.parsers import FormParser, JSONParser
from rest_framework.response import Response

from easyvibe.settings import LATEST_VERSION
from evb_rest.authentication import EasyvibeAuthentication
from evb_rest.models import Device


__author__ = 'Usachev'

logger = logging.getLogger('web_logger')

class DeviceViewSetV2(viewsets.ViewSet):
    authentication_classes = (EasyvibeAuthentication,)
    parser_classes = (FormParser, JSONParser)
    model = Device

    def create(self, request):
        token = request.DATA.get('token')
        locale = request.DATA.get('locale')
        ver = request.DATA.get('version')
        bundle = request.DATA.get('bundle_id')

        logger.info('Device token change called by id = %s' % request.user.id)
        try:
            cursor = connection.cursor()
            query = "UPDATE evb_devices SET token=NULL WHERE token='%s' AND user_id != %d" % (token.encode('utf-8'),request.user.id)
            cursor.execute(query)
            connection.commit()
            dev = Device.objects.get(user_id=request.user.id)
            dev.token = token
            if locale is not None:
                dev.locale = locale
            if ver is not None:
                logger.info('Running version:%s ' % ver)
                dev.client_version = ver
            if bundle is not None:
                dev.client_bundle = bundle
            dev.save(force_update=True)
            logger.info('Device token updated')
            return Response({'token': dev.token, 'locale': dev.locale, 'latest_version': LATEST_VERSION},
                            status=status.HTTP_200_OK)
        except Device.DoesNotExist:
            return Response({'error': 'user device is not registered'}, status=status.HTTP_400_BAD_REQUEST)