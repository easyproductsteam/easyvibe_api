from datetime import timedelta
from django.db import connection
from django.utils import timezone
import json
import logging
from rest_framework import viewsets, status
from rest_framework.decorators import *
from rest_framework.parsers import JSONParser, FormParser
from rest_framework.response import Response
from time import mktime

from easyvibe.settings import VIBES_MAX_QTY, STEP1_INTERVAL
from evb_rest.authentication import EasyvibeAuthentication
from evb_rest.models import User, Song, Recommendation, Dialog, Device, Notification, OfferSet, Offer
from evb_rest.tasks import request_offer, request_award_offers


__author__ = 'Usachev'

logger = logging.getLogger('web_logger')

class VibeViewSetV2(viewsets.ViewSet):
    authentication_classes = (EasyvibeAuthentication,)
    parser_classes = (FormParser,)
    model = User

    @action(methods=('POST',))
    def invite(self, request, pk=None):
        logger.info('Mark invite as send method has been called by id = %s' % request.user.id)
        if request.user.do_invite:
            try:
                award_offer_sets = OfferSet.objects.filter(user_id=request.user.id, used_at__isnull=True, type=2).count()
                if award_offer_sets == 0:
                    request_award_offers(request.user.id, 25)
                request.user.do_invite = False
                request.user.save(force_update=True)
                return Response(status=status.HTTP_200_OK)
            except BaseException, e:
                return Response({'error':e.message},status=status.HTTP_400_BAD_REQUEST)
        else:
            return  Response(status=status.HTTP_304_NOT_MODIFIED)


    @action(methods=('POST',))
    def set_counter(self, request, pk=None):
        #logger.info('Alter vibe counter method has been called by id = %s' % request.user.id)
        value = int(pk)
        do_multivibe = request.user.do_multivibe
        do_invite = request.user.do_invite
        tz_aware_time = timezone.now()
        do_upload = False
        offer_sets = None


        if request.user.vibe_used_at != None:
            t_delta = tz_aware_time - request.user.vibe_used_at
            logger.info('User Id = %d -> last vibe sent at: %s, T-delta: %s min.' % (request.user.id,request.user.vibe_used_at,t_delta.seconds/60))
            #x_delta = timedelta(hours=request.user.recharge_timeout.hour)
            x_delta = timedelta(minutes=request.user.recharge_timeout.minute)
            if t_delta > x_delta:
            #if t_delta > x_delta and request.user.vibes_qty == 0:
                offer_sets = OfferSet.objects.filter(user_id=request.user.id, used_at__isnull=True).order_by('id').count()
                if offer_sets < 1:
                    do_upload = True
                request.user.recharge_timeout = STEP1_INTERVAL
                if do_upload:
                    if request.user.current_step < 10:
                        request.user.current_step +=1
                    else:
                        request.user.current_step = 1

                    if request.user.current_step in range(1,10,2):
                        request.user.do_invite = True

                request.user.step_init = False
                request.user.step_init_at = None
                request.user.is_notified = False
                request.user.vibes_qty = request.user.vibes_max_qty
        else:
            #do the same as Step 1, if User is new
            request.user.vibes_max_qty = VIBES_MAX_QTY * 0.4
            request.user.current_step = 1
            request.user.do_invite = False
            request.user.do_multivibe = False
            request.user.recharge_timeout = STEP1_INTERVAL

        if do_upload and not request.user.do_invite:
            logger.debug('Package not found, creating new one...')
            request_offer(request.user.id)
            logger.debug('Package created...')
            offer_set_q = OfferSet.objects.filter(user_id=request.user.id, used_at__isnull=True).order_by('id')[0]
            vibes_max_qty = offer_set_q.qty
            vibes_qty = vibes_max_qty
        else:
            offer_set_q = OfferSet.objects.filter(user_id=request.user.id, used_at__isnull=True).order_by('id')
            if len(offer_set_q) > 0:
                offer_set = offer_set_q[0]
                vibes_qty = Offer.objects.filter(oset_id = offer_set.id, used_at__isnull=True).count()
                vibes_max_qty = offer_set.qty
            else:
                vibes_qty = 0
                vibes_max_qty = 0

        #todo: optimize database access queries
        request.user.is_notified = False
        request.user.save()
        logger.debug('Vibes total: %d\t vibes left: %d\tMultivibe:%s\tInvite:%s' % (vibes_max_qty, vibes_qty, do_multivibe, do_invite))
        return Response({'vibes_total': vibes_max_qty,
                         'vibes_left': vibes_qty,
                         'step': request.user.current_step,
                         'multivibe': do_multivibe,
                         'invite': do_invite}, status=status.HTTP_200_OK)


class MultivibeViewSetV2(viewsets.ViewSet):
    authentication_classes = (EasyvibeAuthentication,)
    parser_classes = (FormParser, JSONParser)
    model = User

    @action(methods=('POST',))
    def skip(self, request, pk=None):
        try:
            logger.info("Skip multivibe method called")
            request.user.do_multivibe = False
            request.user.current_step += 1
            request.user.save(force_update=True)
            logger.info("Multivibe for user ID=%d has been cancelled" % request.user.id)
            return Response(status=status.HTTP_200_OK)
        except BaseException, e:
            logger.error('SQL Exception: %s' % e.message)
            return Response({'error':e.message},status=status.HTTP_400_BAD_REQUEST)

    def create(self,request,pk=None):
        try:
            logger.info('Multivibe method called by ID=%d' % request.user.id)
            if request.user.do_multivibe == True:
                song_hash = request.DATA.get('song_hash')
                if song_hash is None:
                    return Response({'error':'Song hash is not present'},status=status.HTTP_400_BAD_REQUEST)
                try:
                    song = Song.objects.get(hash=song_hash)
                except Song.DoesNotExist:
                    logger.warning('Song with hash=%s does not exists' % song_hash)
                    return Response({'error':'Song not found'},status=status.HTTP_204_NO_CONTENT)


                dbcur = connection.cursor()
                # user's statistical info
                current_user = User.objects.get(id=request.user.id)
                dbcur = connection.cursor()
                query = 'select count(1) from evb_recommendations where (creator_id = %d or recipient_id=%d) and status IN (1,2)' % (
                    current_user.id, current_user.id)
                logger.info('Executing SQL: %s' % query)
                dbcur.execute(query)
                res = dbcur.fetchone()
                unread_qty = res[0]

                query = 'select count(1) from evb_recommendations where (creator_id = %d or recipient_id=%d) and dialog_id is NULL  and status IN (1,2)' % (
                    current_user.id, current_user.id)
                logger.info('Executing SQL: %s' % query)
                dbcur.execute(query)
                res = dbcur.fetchone()
                unread_out_of_dialog = res[0]

                query = 'select count(1) from evb_recommendations where (creator_id = %d or recipient_id=%d) and dialog_id is not NULL  and status IN (1,2)' % (
                    current_user.id, current_user.id)
                logger.info('Executing SQL: %s' % query)
                dbcur.execute(query)
                res = dbcur.fetchone()
                unread_in_dialog = res[0]

                #finding users for multivibe
                query='''
                        SELECT evb_users.id
                        FROM evb_users
                        WHERE age >=%d
                        AND age <%d
                        AND gender != %d
                        ORDER by  Rand() LIMIT 0,25''' % (request.user.age -4,request.user.age+4,request.user.gender)
                logger.info("SQL query: %s" % query)
                dbcur.execute(query)
                for u in dbcur.fetchall():
                    try:
                        chosen_user = User.objects.get(id=u[0])
                        logger.info("Sending multivibe to %s (%d)" % (chosen_user.name,chosen_user.id))
                        r = Recommendation(creator_id=request.user.id, recipient_id=chosen_user.id, song_id=song.id)
                        # check whatever sender and recipient dialog exists
                        try:
                            user_dialog = Dialog.objects.get(user_id=chosen_user.id, companion_id=request.user.id)
                            r.dialog_id = user_dialog.id
                            r.status = 2
                        except Dialog.DoesNotExist:
                            pass
                        r.save()

                        push_args = {}
                        push_args['loc_key'] = 'NEW_VIBE_KEY'
                        push_args['quantity'] = unread_qty
                        custom_data = {
                            'created': mktime(r.created_at.timetuple()),
                            'companion_id': request.user.id,
                            'ci': unread_out_of_dialog,
                            'cr': unread_in_dialog
                        }

                        if r.dialog_id is not None:
                            custom_data['dialog_id'] = r.dialog_id
                        push_args['custom_data'] = custom_data

                        user_device = Device.objects.filter(user_id=chosen_user.id).first()
                        if user_device is not None and user_device.token is not None:
                            n = Notification(device_id=user_device.id, push_args=json.dumps(push_args))
                            n.save()
                            logger.info('Notification for device %s queued' % user_device.id)
                        else:
                            logger.error('Device for user %s is not registered, unable to send notification' % chosen_user.id)
                    except User.DoesNotExist:
                        pass
                dbcur.close()
                logger.info("Multivibe has been sent!")
                logger.info("Extra package of people created")
                request_award_offers(request.user.id, 8)
                request.user.do_multivibe = False
                request.user.save()
                return Response(status=status.HTTP_200_OK)
            else:
                return Response({'error':'Multivibe flag was not set'},status=status.HTTP_406_NOT_ACCEPTABLE)
        except BaseException, e:
            logger.error("Can't send multivibe: %s" % e.message)
            return Response({'error':e.message},status=status.HTTP_400_BAD_REQUEST)

