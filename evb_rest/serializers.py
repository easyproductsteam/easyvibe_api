__author__ = 'Usachev'

from rest_framework import serializers

from models import *


class NotNullableCharField(serializers.Field):
    def to_native(self, value):
        if value is None:
            return ''
        else:
            return value

class NotNullableIntField(serializers.Field):
    def to_native(self, value):
        if value is None:
            return 0
        else:
            return value


class UserSerializer(serializers.ModelSerializer):
    already_exist = serializers.BooleanField(default=False, source='user_exists')
    user_name = serializers.CharField(source='name')
    fb_id = NotNullableCharField(source='facebook_id')
    vk_id = NotNullableIntField(source='vk_id')
    age = serializers.IntegerField(source='count_age')
    lang = serializers.CharField(source='language')
    picture = NotNullableCharField(source='pic')

    class Meta:
        model = User
        fields = ('id', 'user_name', 'age', 'gender', 'fb_id', 'vk_id', 'already_exist', 'lang', 'picture')


class SongSerializer(serializers.ModelSerializer):
    artwork = NotNullableCharField(source='artwork')
    url = NotNullableCharField(source='url')
    itunes_url = NotNullableCharField(source='itunes_url')

    class Meta:
        model = Song
        fields = ('id', 'hash', 'author', 'title', 'genre', 'artwork', 'url','itunes_url')


class DialogSerializer(serializers.ModelSerializer):
    created_at = serializers.DateTimeField(default=None, source='get_created_at')
    direction = serializers.IntegerField(default=0, source='get_direction')

    class Meta:
        model = SongView
        fields = ('author', 'title', 'genre', 'artwork', 'url', 'created_at', 'direction')

class CounterSerializer(serializers.ModelSerializer):
    updated_at = serializers.DateTimeField(source='vibe_counter_timestamp')

    class Meta:
        model = User
        fields = ('vibes_qty','vibes_max_qty','updated_at')






