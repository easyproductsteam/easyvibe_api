from unittest import TestCase,main


# Create your tests here.
from tasks import request_offer


class UserPackageFeedTestCase(TestCase):

    qty = 25
    test_user=1
    package=[]

    def setUp(self):
        self.package = request_offer(1,self.qty,in_test_case=True)


    def test_package_size(self):
        self.assertEqual(len(self.package),self.qty,'Packet length is not equals an expected one')


if __name__ == "__main__":
    main()